from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response
from portal.models import *
from datetime import datetime
from django.core import serializers
import json


@login_required
def index(request):
    context = RequestContext(request)
    context_dict = {'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Keeping Track of Wildlife',
    }
    dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
    uo = request.user
    context_dict['user'] = uo
    up = UserProfile.objects.get(user=uo)

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    # Prepare the context
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list
    return render_to_response('query.html', context_dict, context)


@login_required
def json_get_model_fieldlist(request, tbl='A'):
    """
    This returns a JSON object of the model's field list for
    query builder to populate dropdowns
    """
    if tbl == 'A':
        out = Animal.get_fieldlist()
    elif tbl == 'L':
        out = Log.get_fieldlist()
    elif tbl == 'M':
        out = Mark.get_fieldlist()
    elif tbl == 'B':
        out = Breeding.get_fieldlist()
    return HttpResponse(json.dumps(out), content_type='application/json')


@login_required
def json_get_attribute_fieldlist(request, tbl='A'):
    """
    This returns a JSON object of the attribute fields for query
    builder to populate dropdowns
    """
    out = AttributeField.get_fieldlist(tbl, request.session.get('cur_ds'))
    return HttpResponse(json.dumps(out), content_type="application/json")


@login_required
@csrf_exempt
def json_query(request):
    """
    This is the main query routine that gets a query object from a POST request
    and figures out what to return
    """
    out = {}
    if request.is_ajax() and request.method == 'POST':
        try:
            q = json.loads(request.body)
            ds = AnimalDataset.objects.get(id=request.session['cur_ds'])
            out['results'] = execute_query(q['query'], ds)
        except Exception, e:
            out['errors'] = []
            out['errors'].append("Unable to parse JSON Request: " + str(e))
        return HttpResponse(json.dumps(out), content_type="application/json")
    else:
        out['errors'] = []
        out['errors'].append("Illegal request (not AJAX AND POST")
        return HttpResponse(json.dumps(out), content_type="application/json")

def is_attribute(tbl,ds, field):
    att = AttributeField.objects.filter(table=tbl, dataset=ds)
    if len(att) == 0:
        att = AttributeField.objects.filter(table=tbl, is_global=True)
    for a in att:
        if a.name == field:
            return True
    return False

def get_attribute_value(tbl, ds, field, obj):
    att = AttributeField.objects.filter(table=tbl, dataset=ds)
    if len(att) == 0:
        att = AttributeField.objects.filter(table=tbl, is_global=True)

    attfield = None
    for a in att:
        if a.name == field:
            attfield = a
    if tbl == 'Animal':
        try:
            this_attribute = AnimalAttribute.objects.get(animal=obj, field=attfield)
        except AnimalAttribute.DoesNotExist:
            return ''	# attribute does not exist, return blank string
    elif tbl == 'Mark':
        try:
            this_attribute = MarkAttribute.objects.get(animal=obj, field=attfield)
        except MarkAttribute.DoesNotExist:
            return ''	# attribute does not exist, return blank string
    elif tbl == 'Log':
        try:
            this_attribute = LogAttribute.objects.get(animal=obj, field=attfield)
        except LogAttribute.DoesNotExist:
            return ''	# attribute does not exist, return blank string
    elif tbl == 'Breeding':
        try:
            this_attribute = BreedingAttribute.objects.get(animal=obj, field=attfield)
        except BreedingAttribute.DoesNotExist:
            return ''	# attribute does not exist, return blank string

    if attfield.type == 'L':
        return this_attribute.field_value.shortval
    elif attfield.type == 'T':
        return this_attribute.text_value
    elif attfield.type == 'F':
        return this_attribute.float_value
    elif attfield.type == 'I':
        return this_attribute.int_value
    elif attfield.type == 'D':
        return this_attribute.time_value.strftime('%Y-%m-%d %H:%M:%S')

"""
A whole bunch of functions to put together a query resultset
"""


def getquery_animal_attributes(q, ds):
    return None	# for now, an empty set

def getquery_animal(q, ds, aset=None):
    animal_set = set()

    using_fields = False
    using_attributes = False
    # Must always filter on the dataset
    a = Animal.objects.filter(dataset=ds)
    for w in q:
        if w['table'] == 'Animal':
            if not is_attribute('A', ds, w['field']):
                using_fields = True
                cmp = w['field']
                if w['operation'] == 'equals':
                    cmp += '__exact'
                    a = a.filter(**{cmp:w['filter']})
                elif w['operation'] == 'not equal':
                    cmp += '__exact'
                    a = a.exclude(**{cmp:w['filter']})
                elif w['operation'] == 'contains':
                    cmp += '__contains'
                    a = a.filter(**{cmp:w['filter']})
                elif w['operation'] == 'does not contain':
                    cmp += '__contains'
                    a = a.exclude(**{cmp:w['filter']})
                #TODO - more operations
            else:
                using_attributes = True
    
    # Add list of IDs to the set from all the filters above
    try:
        for r in a:
            animal_set.add(r.id)
    except Animal.DoesNotExist:
        pass

    # intersection between attributes and this list will be the combined animal query
    if using_attributes and using_fields:
        att_set = getquery_animal_attributes(q, ds)
        if att_set is not None:
            animal_set.intersection_update(att_set)
    elif using_attributes:
        # not using any fields in the lookup, so only rely on attributes
        att_set = getquery_animal_attributes(q, ds)
        if att_set is not None:
            animal_set = att_set
    elif using_fields:
        pass	# just use the base animal_set
    
    # finally intersection between previously compiled animal set and this will be
    # the combined resultset of id's to lookup
    if aset is not None:
        animal_set.intersection_update(aset)

    return animal_set

def getquery_mark_attributes(q, ds):
    return set()	# empty set for now

def getquery_mark(q, ds):
    mark_set = set()
    animal_set = set()
    
    using_fields = False
    using_attributes = False
    using_conditions = False
    m = Mark.objects.filter()
    for w in q:
        if w['table'] == 'Mark' and w['operation'] != 'display' and w['operation'] != 'blank':
            if not is_attribute('M', ds, w['field']):
                using_fields = True
                cmp = w['field']
                if w['operation'] == 'equals':
                    cmp += '__exact'
                    m = m.filter(**{cmp:w['filter']})
                elif w['operation'] == 'not equal':
                    cmp += '__exact'
                    m = m.exclude(**{cmp:w['filter']})
                elif w['operation'] == 'contains':
                    cmp += '__contains'
                    m = m.filter(**{cmp:w['filter']})
                elif w['operation'] == 'does not contain':
                    cmp += '__contains'
                    m = m.exclude(**{cmp:w['filter']})
                #TODO - more operations
            else:
                using_attributes = True
    
    try:
        if using_fields:
            for r in m:
                mark_set.add((r.id, r.animal.id))
    except Mark.DoesNotExist:
        pass
    
    # intersection between attributes if used
    mset = set()
    if using_attributes and using_fields:
        att_set = getquery_mark_attributes(q, ds)
        for x in mark_set:
            if x[0] in att_set:
                animal_set.add(x[1])
                mset.add(x[0])
    elif using_attributes:
        att_set = getquery_mark_attributes(q, ds)
        for x in att_set:
            m = Mark.objects.get(id=x)
            animal_set.add(m.animal.id)
            mset.add(x)
    elif using_fields:
        for x in mark_set:
            animal_set.add(x[1])
            mset.add(x[0])
    else:
        return (None, None)	# only 'display' and 'blank' operations
    
    return (animal_set, mset)

def getquery_log_attributes(q, ds):
    return set()	# empty set for now

def getquery_breeding(q, ds):
    return set()	# empty set for now

def getquery_log(q, ds):
    log_set = set()
    animal_set = set()
    
    using_fields = False
    using_attributes = False
    using_breeding = False
    l = Log.objects.filter()
    for w in q:
        if w['table'] == 'Log' and w['operation'] != 'blank' and w['operation'] != 'display':
            if not is_attribute('L', ds, w['field']):
                using_fields = True
                cmp = w['field']
                if w['operation'] == 'equals':
                    cmp += '__exact'
                    l = l.filter(**{cmp:w['filter']})
                elif w['operation'] == 'not equal':
                    cmp += '__exact'
                    l = l.exclude(**{cmp:w['filter']})
                elif w['operation'] == 'contains':
                    cmp += '__contains'
                    l = l.filter(**{cmp:w['filter']})
                elif w['operation'] == 'does not contain':
                    cmp += '__contains'
                    l = l.exclude(**{cmp:w['filter']})
                #TODO - more operations
            else:
                using_attributes = True
        if w['table'] == 'Breeding':
            using_breeding = True
    
    try:
        if using_fields:
            for r in l:
                log_set.add((r.id, r.animal.id))
    except Log.DoesNotExist:
        pass
        
    # intersection between atributes if used
    breeding_set = set()
    if using_breeding:
        breeding_set = getquery_breeding(q, ds)
    
    lset = set()
    if using_attributes and using_fields:
        att_set = getquery_log_attributes(q, ds)
        if using_breeding:
            att_set.intersection_update(breeding_set)
        for x in log_set:
            if x[0] in att_set:
                animal_set.add(x[1])
                lset.add(x[0])
    elif using_attributes:
        att_set = getquery_log_attributes(q, ds)
        if using_breeding:
            att_set.intersection_update(breeding_set)
        for x in att_set:
            l = Log.objects.get(id=x)
            animal_set.add(l.animal.id)
            lset.add(x)
    elif using_fields:
        for x in log_set:
            if using_breeding:
                if x[0] in breeding_set:
                    animal_set.add(x[1])
                    lset.add(x[0])
            else:
                animal_set.add(x[1])
                lset.add(x[0])
    else:
        return (None, None)	# only 'blank' or 'display' operations
        
    return (animal_set, lset)


def execute_query(q, ds):
    resultset = []
    tables = {}
    # Determine which tables are used in the query
    for w in q:
        tables[w['table']] = True
        
    # If we're looking for breeding data then that's a log!
    if 'Breeding' in tables:
        tables['Log'] = True
    
    result_animals = set()
    if 'Mark' in tables and 'Log' in tables:
        (log_animals_set, log_set)  = getquery_log(q, ds)
        (mark_animals_set, mark_set) = getquery_mark(q, ds)
        if log_animals_set is None and mark_animals_set is None:
            result_animals = getquery_animal(q, ds)
        elif log_animals_set is None:
            result_animals = getquery_animal(q, ds, aset=mark_animals_set)
        elif mark_animals_set is None:
            result_animals = getquery_animal(q, ds, aset=log_animals_set)
        else:
            animal_set = mark_animals_set.intersection(log_animals_set)
            result_animals = getquery_animal(q, ds, aset=animal_set)
    elif 'Log' in tables:
        (animal_set, log_set)  = getquery_log(q, ds)
        if animals_set is None:
            result_animals = getquery_animal(q, ds)
        else:
            result_animals = getquery_animal(q, ds, aset=animal_set)
    elif 'Mark' in tables:
        (animal_set, mark_set) = getquery_mark(q, ds)
        if animal_set is None:
            result_animals = getquery_animal(q, ds)
        else:
            result_animals = getquery_animal(q, ds, aset=animal_set)
    elif 'Animal' in tables:
        result_animals = getquery_animal(q, ds)

    # Compile the results into a JSON array
    resultset = []

    if 'Mark' in tables and 'Log' in tables:
        a = Animal.objects.filter(pk__in=result_animals)
        l = Log.objects.filter(animal__in=a)
        if log_set is not None:
            l = l.filter(pk__in=log_set)
        m = Mark.objects.filter(animal__in=a)
        if mark_set is not None:
            m = m.filter(pk__in=mark_set)
        for lr in l:
            result = []
            result.append(lr.animal.id)	# first field is special
            for w in q:
                if w['table'] == 'Animal':
                    if is_attribute('A', ds, w['field']):
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(get_attribute_value('Animal', ds, w['field'], lr.animal))                            
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(lr.animal, w['field']))
                elif w['table'] == 'Log':
                    if is_attribute('Mark', ds, w['field']):
                        pass # TODO
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(lr, w['field']))
                elif w['table'] == 'Mark':
                    if is_attribute('Mark', ds, w['field']):
                        pass # TODO
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            tm = m.filter(animal=lr.animal)
                            result.append(getattr(tm[0], w['field']))
            resultset.append(result)
    elif 'Log' in tables:
        a = Animal.objects.filter(pk__in=result_animals)
        l = Log.objects.filter(animal__in=a)
        if log_set is not None:
            l = l.filter(pk__in=log_set)
        for lr in l:
            result = []
            result.append(lr.animal.id)	# first field is special
            for w in q:
                if w['table'] == 'Animal':
                    if is_attribute('Animal', ds, w['field']):
                        pass # TODO
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(lr.animal, w['field']))
                elif w['table'] == 'Log':
                    if is_attribute('Mark', ds, w['field']):
                        pass # TODO
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(lr, w['field']))
            resultset.append(result)
    elif 'Mark' in tables:
        a = Animal.objects.filter(pk__in=result_animals)
        m = Mark.objects.filter(animal__in=a)
        if mark_set is not None:
            m = m.filter(pk__in=mark_set)
        for mr in m:
            result = []
            result.append(mr.animal.id)	# first field is special
            for w in q:
                if w['table'] == 'Animal':
                    if is_attribute('Animal', ds, w['field']):
                        pass # TODO
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(mr.animal, w['field']))
                elif w['table'] == 'Mark':
                    if is_attribute('Mark', ds, w['field']):
                        pass # TODO
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(mr, w['field']))
            resultset.append(result)
    elif 'Animal' in tables:
        a = Animal.objects.filter(pk__in=result_animals)
        for r in a:
            result = []
            result.append(r.id)	# first field is special
            for w in q:
                if w['table'] == 'Animal':
                    if is_attribute('A', ds, w['field']):
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(get_attribute_value('Animal', ds, w['field'], r))                            
                    else:
                        if w['operation'] == 'blank':
                            result.append('')
                        else:
                            result.append(getattr(r, w['field']))
                #TODO - other tables                    
            resultset.append(result)
    return resultset

@login_required
@csrf_exempt
def json_query_save(request):
    """
    Responds to the Save Query modal dialog
    :param request: Django request object
    :return: HttpResponse in JSON
    """
    out = {}
    if request.is_ajax() and request.method == 'POST':
        try:
            # Grab the dataset
            uo = request.user
            up = UserProfile.objects.get(user=uo)
            # Grabs current dataset (if not available set it)
            if (request.session.get('cur_ds') is None):
                if (up.default_dataset is None):
                    request.session['cur_ds'] = 1
                else:
                    request.session['cur_ds'] = up.default_dataset.pk

            q = json.loads(request.body)
            date_created = datetime.now()
            created_by = request.user
            name = q['name']
            ds = AnimalDataset.objects.get(id=request.session['cur_ds'])
            #TODO VALIDATE QUERY BEFORE SAVING
            #TODO CHECK NAME BEFORE SAVING AND REPLACE/UPDATE IF NECESSARY
            del q['name']
            nq = Queries(date_created=date_created,
                         created_by=created_by,
                         name=name,
                         serialised_query = json.dumps(q),
                         dataset=ds)
            nq.save()
            out['success'] = "Query Saved."
        except Exception, e:
            out['errors'] = []
            out['errors'].append("Unable to parse JSON request: " + str(e))
    else:
        out['errors'] = []
        out['errors'].append("Illegal request (not AJAX AND POST")
    return HttpResponse(json.dumps(out), content_type="application/json")

@login_required
def json_load_query_list(request):
    """
    Loads a JSON of the list of available queries
    :param request: Django request object
    :return: HttpResponse in JSON of list of available queries for the dataset
    """
    # Grab the dataset
    uo = request.user
    up = UserProfile.objects.get(user=uo)
    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk
    out = []
    ds = AnimalDataset.objects.get(id=request.session['cur_ds'])
    queries = Queries.objects.filter(dataset=ds)
    try:
        for r in queries:
            out.append({'name': r.name, 'created_by': r.created_by.username, 'date_created': r.date_created.strftime('%Y-%m-%d %H:%M:%S'), 'id': r.id})
    except Queries.DoesNotExist:
        pass
    return HttpResponse(json.dumps(out), content_type="application/json")

@login_required
def json_load_query(request, qid=None):
    """
    Loads a specific query as a JSON object
    :param request: Django request object
    :param qid: Query ID (integer)
    :return: HttpResponse in JSON of serialized query
    """
    out = {}
    if qid is None:
        out['errors'] = []
        out['errors'].append('You need to specify a query to load')
        return HttpResponse(json.dumps(out), content_type="application/json")

    try:
        q = Queries.objects.get(id=qid)
        out = json.loads(q.serialised_query)
    except Queries.DoesNotExist:
        out['errors'] = []
        out['errors'].append('Query does not exist')

    return HttpResponse(json.dumps(out), content_type="application/json")

@login_required
def json_delete_query(request, qid=None):
    """
    Loads a specific query as a JSON object
    :param request: Django request object
    :param qid: Query ID (integer)
    :return: HttpResponse in JSON of serialized query
    """
    out = {}
    if qid is None:
        out['errors'] = []
        out['errors'].append('You need to specify a query to delete')
        return HttpResponse(json.dumps(out), content_type="application/json")

    try:
        q = Queries.objects.get(id=qid)
        q.delete()
        q.save()
        out['success'] = 'Query deleted.'
    except Queries.DoesNotExist:
        out['errors'] = []
        out['errors'].append('Query does not exist')

    return HttpResponse(json.dumps(out), content_type="application/json")
