from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from portal.models import *
from datetime import datetime
import json

@login_required
def new_animal(request):
    # This stuff is on pretty much every page, should really put it in it's own function
    context = RequestContext(request)
    context_dict = { 'pagetitle':	'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
    dataset_list = AnimalDataset.objects.order_by('name')

    # shows user details
    uo = request.user
    context_dict['user']= uo
    up = UserProfile.objects.get(user=uo)

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    # Prepare the context
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    # Generate the data for a blank form (eg dropdown lists, attribute lists)
    # We need to lookup the mandatory fields for the dataset and globally
    min_animal_att_fields = []
    att_field_list = AttributeField.objects.filter(table='A', is_global=True, is_mandatory=True)
    for x in att_field_list:
        min_animal_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='A', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
        min_animal_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    context_dict['min_animal_att_fields'] = min_animal_att_fields

    # Look up all fields that can be added
    all_animal_att_fields = []
    animal_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='A', is_global=True)
    for x in att_field_list:
        all_animal_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='A', dataset=cur_ds)
    for x in att_field_list:
        all_animal_att_fields.append(x)

    context_dict['animal_att_field_values'] = animal_att_field_values
    context_dict['all_animal_att_fields'] = all_animal_att_fields

    # Do the same for mark attributes
    # We need to lookup the mandatory fields for the dataset and globally
    min_mark_att_fields = []
    att_field_list = AttributeField.objects.filter(table='M', is_global=True, is_mandatory=True)
    for x in att_field_list:
        min_mark_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
        min_mark_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    context_dict['min_mark_att_fields'] = min_mark_att_fields
    # Look up all fields that can be added
    all_mark_att_fields = []
    att_field_list = AttributeField.objects.filter(table='M', is_global=True)
    for x in att_field_list:
        all_mark_att_fields.append(x)
        att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds)
        for x in att_field_list:
            all_mark_att_fields.append(x)

    context_dict['all_mark_att_fields'] = all_mark_att_fields

    # Render the form
    return render_to_response('newanimal.html', context_dict, context)

##########################################################################################################################
@login_required
def new_animal_wizard(request):
    # This stuff is on pretty much every page, should really put it in it's own function
    context = RequestContext(request)
    context_dict = { 'pagetitle':	'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
    dataset_list = AnimalDataset.objects.order_by('name')

    # shows user details
    uo = request.user
    context_dict['user']= uo
    up = UserProfile.objects.get(user=uo)

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    # Prepare the context
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list
    
    
    mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
    if not 'error' in mtypes:
      context_dict['mark_types'] = mtypes['mark_types']

    # Generate the data for a blank form (eg dropdown lists, attribute lists)
    # We need to lookup the mandatory fields for the dataset and globally
    min_animal_att_fields = []
    att_field_list = AttributeField.objects.filter(table='A', is_global=True, is_mandatory=True)
    for x in att_field_list:
        min_animal_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='A', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
        min_animal_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    context_dict['min_animal_att_fields'] = min_animal_att_fields

    # Look up all fields that can be added
    all_animal_att_fields = []
    animal_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='A', is_global=True)
    for x in att_field_list:
        all_animal_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='A', dataset=cur_ds)
    for x in att_field_list:
        all_animal_att_fields.append(x)

    context_dict['animal_att_field_values'] = animal_att_field_values
    context_dict['all_animal_att_fields'] = all_animal_att_fields

    # Do the same for mark attributes
    # We need to lookup the mandatory fields for the dataset and globally
    min_mark_att_fields = []
    att_field_list = AttributeField.objects.filter(table='M', is_global=True, is_mandatory=True)
    for x in att_field_list:
        min_mark_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
        min_mark_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    context_dict['min_mark_att_fields'] = min_mark_att_fields

    # Look up all fields that can be added
    all_mark_att_fields = []
    mark_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='M', is_global=True)
    for x in att_field_list:
        all_mark_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds)
    for x in att_field_list:
        all_mark_att_fields.append(x)

    context_dict['mark_att_field_values'] = mark_att_field_values
    context_dict['all_mark_att_fields'] = all_mark_att_fields
    
    # Do the same for log attributes
    # We need to lookup the mandatory fields for the dataset and globally
    min_log_att_fields = []
    att_field_list = AttributeField.objects.filter(table='L', is_global=True, is_mandatory=True)
    for x in att_field_list:
        min_log_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
        min_log_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    context_dict['min_log_att_fields'] = min_log_att_fields

    # Look up all fields that can be added
    all_log_att_fields = []
    log_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='L', is_global=True)
    for x in att_field_list:
        all_log_att_fields.append(x)
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds)
    for x in att_field_list:
        all_log_att_fields.append(x)

    context_dict['log_att_field_values'] = log_att_field_values
    context_dict['all_log_att_fields'] = all_log_att_fields

    # Render the form
    return render_to_response('newanimal-wizard.html', context_dict, context)

@login_required
@csrf_exempt
def json_add_new_animal(request):
    out = {}
    if request.is_ajax() and request.method == 'POST':
#        try:
        a = json.loads(request.body)
        uo = request.user
        # Grab current dataset
        cur_ds = AnimalDataset.objects.get(pk=request.session['cur_ds'])

        # FORM VALIDATION
        animal_gender = a['gender']
        animal_comments = a['comments']
        mark_type = a['mark']['type']
        mark_number = a['mark']['number']
        mark_is_present = a['mark']['is_present']
        mark_comments = a['mark']['comments']
        # THIS IS WRONG - NEED TO VALIDATE DATE!!
        mark_date_attached = datetime.strptime(a['mark']['date_attached'], '%d-%m-%Y %H:%M:%S')

        # Need to ensure that there are adequate animal and mark fields

        # Need to ensure valid Animal Attributes

        # Need to ensure valid Mark Attributes

        # Validation Passed, add a record!
        # Get current date and time
        now = datetime.now()
        # Create an animal
        animal = Animal.objects.create(dataset=cur_ds, gender=animal_gender, comments=animal_comments, date_created=now, date_modified=now, created_by=uo, modified_by=uo)

        # Create animal attributes
        for att in a['attributes']:
            # grab the attribute
            af = AttributeField.objects.filter(is_global=True, table='A', name=att)
            if len(af) == 1:
                afield = af[0]
            elif len(af) == 0:
                af = AttributeField.objects.filter(is_global=False, table='A', dataset=cur_ds, name=att)
                if len(af) != 1:
                    raise Exception("WTF dataset animal attribute field not found")
                else:
                    afield = af[0]
            else:
                raise Exception("WTF 2 global animal attribute fields with the same name")

            # if lookup, grab the value
            if afield.type == 'L':
                afv = AttributeFieldValue.objects.get(shortval=a['attributes'][att], field=afield)
                # returning any more than 1 value will raise an exception which is good
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, field_value=afv)
            # not a lookup, just store the value in the right field
            elif afield.type == 'F':
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, float_value=a['attributes'][att])
            elif afield.type == 'T':
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, text_value=a['attributes'][att])
            elif afield.type == 'I':
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, int_value=a['attributes'][att])
            elif afield.type == 'D':
                d = datetime.strptime(a['attributes'][att], '%d-%m-%Y %H:%M:%S')
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, time_value=d)

        # Create a log
        log = Log.objects.create(animal=animal, log_date=now, log_type='R', season='', comments='New Animal Entered', sample_taken=False, date_created=now, date_modified=now, created_by=uo, modified_by=uo)

        # Create a mark
        mark = Mark.objects.create(dataset=cur_ds, animal=animal, mark_number=mark_number, mark_type=mark_type, is_present=mark_is_present, log_attached=log, comments=mark_comments, date_created=now, date_modified=now, created_by=uo, modified_by=uo, date_attached=mark_date_attached)

        # Create mark attributes
        for att in a['mark']['attributes']:
            # grab the attribute
            af = AttributeField.objects.filter(is_global=True, table='M', name=att)
            if len(af) == 1:
                afield = af[0]
            elif len(af) == 0:
                af = AttributeField.objects.filter(is_global=False, table='M', dataset=cur_ds, name=att)
                if len(af) != 1:
                    raise Exception("WTF dataset mark attribute field not found")
                else:
                    afield = af[0]
            else:
                raise Exception("WTF 2 global mark attribute fields with the same name")

            # if lookup, grab the value
            if afield.type == 'L':
                afv = AttributeFieldValue.objects.get(shortval=a['mark']['attributes'][att], field=afield)
                # returning any more than 1 value will raise an exception which is good
                maa = MarkAttribute.objects.create(mark=mark, field=afield, field_value=afv)
            # not a lookup, just store the value in the right field
            elif afield.type == 'F':
                ma = MarkAttribute.objects.create(mark=mark, field=afield, float_value=a['mark']['attributes'][att])
            elif afield.type == 'T':
                ma = MarkAttribute.objects.create(mark=mark, field=afield, text_value=a['mark']['attributes'][att])
            elif afield.type == 'I':
                ma = MarkAttribute.objects.create(mark=mark, field=afield, int_value=a['mark']['attributes'][att])
            elif afield.type == 'D':
                d = datetime.strptime(a['mark']['attributes'][att], '%d-%m-%Y %H:%M:%S')
                ma = MarkAttribute.objects.create(mark=mark, field=afield, time_value=d)
#        except Exception, e:
#            out['errors'] = []
#            out['errors'].append("Your request caused a processing exception: " + str(e))
#            return HttpResponse(json.dumps(out), content_type="application/json")

        # Successfully created an animal and no exception!
        out['success'] = "Successfully created new animal with id " + str(animal.pk)
    else:
        out['errors'] = []
        out['errors'].append("Illegal request (not AJAX AND POST")
    return HttpResponse(json.dumps(out), content_type="application/json")

#TODO MERGE THE json_new_animal and json_new_animal_wizard functions into a single function!
@login_required
@csrf_exempt
def json_add_new_animal_wizard(request):
    out = {}
    if request.is_ajax() and request.method == 'POST':
#        try:
        a = json.loads(request.body)
        uo = request.user
        # Grab current dataset
        cur_ds = AnimalDataset.objects.get(pk=request.session['cur_ds'])

        # FORM VALIDATION
        animal_gender = a['gender']
        animal_comments = a['comments']
        mark_type = a['mark']['mark_type']
        mark_number = a['mark']['mark_number']
        if (a['mark']['is_present'] == "0"):
          mark_is_present=False
        else:
          mark_is_present=True
        mark_comments = a['mark']['comments']
        # THIS IS WRONG - NEED TO VALIDATE DATE!!
        mark_date_attached = datetime.strptime(a['mark']['date_attached'], '%d-%m-%Y %H:%M:%S')
        log_date = datetime.strptime(a['log']['log_date'], '%d-%m-%Y %H:%M:%S')
        log_comments = a['log']['comments']
        season = a['log']['season']
        if (a['log']['sample_taken'] == "0"):
          sample=False
        else:
          sample=True

        # Need to ensure that there are adequate animal and mark fields

        # Need to ensure valid Animal Attributes

        # Need to ensure valid Mark Attributes

        # Validation Passed, add a record!
        # Get current date and time
        now = datetime.now()
        # Create an animal
        animal = Animal.objects.create(dataset=cur_ds, gender=animal_gender, comments=animal_comments, date_created=now, date_modified=now, created_by=uo, modified_by=uo)

        # Create animal attributes
        for att in a['attributes']:
            # grab the attribute
            af = AttributeField.objects.filter(is_global=True, table='A', name=att)
            if len(af) == 1:
                afield = af[0]
            elif len(af) == 0:
                af = AttributeField.objects.filter(is_global=False, table='A', dataset=cur_ds, name=att)
                if len(af) != 1:
                    raise Exception("WTF dataset animal attribute field not found")
                else:
                    afield = af[0]
            else:
                raise Exception("WTF 2 global animal attribute fields with the same name")

            # if lookup, grab the value
            if afield.type == 'L':
                afv = AttributeFieldValue.objects.get(shortval=a['attributes'][att], field=afield)
                # returning any more than 1 value will raise an exception which is good
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, field_value=afv)
            # not a lookup, just store the value in the right field
            elif afield.type == 'F':
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, float_value=a['attributes'][att])
            elif afield.type == 'T':
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, text_value=a['attributes'][att])
            elif afield.type == 'I':
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, int_value=a['attributes'][att])
            elif afield.type == 'D':
                d = datetime.strptime(a['attributes'][att], '%d-%m-%Y %H:%M:%S')
                aa = AnimalAttribute.objects.create(animal=animal, field=afield, time_value=d)

        # Create a log
        log = Log.objects.create(animal=animal, log_date=log_date, log_type='R', season=season, comments=log_comments, sample_taken=sample, date_created=now, date_modified=now, created_by=uo, modified_by=uo)
        
        # Create log attributes
        for att in a['log']['attributes']:
            # grab the attribute
            af = AttributeField.objects.filter(is_global=True, table='L', name=att)
            if len(af) == 1:
                afield = af[0]
            elif len(af) == 0:
                af = AttributeField.objects.filter(is_global=False, table='L', dataset=cur_ds, name=att)
                if len(af) != 1:
                    raise Exception("WTF dataset log attribute field not found")
                else:
                    afield = af[0]
            else:
                raise Exception("WTF 2 global log attribute fields with the same name")

            # if lookup, grab the value
            if afield.type == 'L':
                afv = AttributeFieldValue.objects.get(shortval=a['log']['attributes'][att], field=afield)
                # returning any more than 1 value will raise an exception which is good
                maa = LogAttribute.objects.create(Log=log, field=afield, field_value=afv)
            # not a lookup, just store the value in the right field
            elif afield.type == 'F':
                la = LogAttribute.objects.create(Log=log, field=afield, float_value=a['log']['attributes'][att])
            elif afield.type == 'T':
                la = LogAttribute.objects.create(Log=log, field=afield, text_value=a['log']['attributes'][att])
            elif afield.type == 'I':
                la = LogAttribute.objects.create(Log=log, field=afield, int_value=a['log']['attributes'][att])
            elif afield.type == 'D':
                d = datetime.strptime(a['log']['attributes'][att], '%d-%m-%Y %H:%M:%S')
                la = LogAttribute.objects.create(Log=log, field=afield, time_value=d)
        
        
        # Create a mark
        mark = Mark.objects.create(animal=animal, mark_number=mark_number, mark_type=mark_type, is_present=mark_is_present, comments=mark_comments, date_created=now, date_modified=now, created_by=uo, modified_by=uo, date_attached=mark_date_attached)

        # Create mark attributes
        for att in a['mark']['attributes']:
            # grab the attribute
            af = AttributeField.objects.filter(is_global=True, table='M', name=att)
            if len(af) == 1:
                afield = af[0]
            elif len(af) == 0:
                af = AttributeField.objects.filter(is_global=False, table='M', dataset=cur_ds, name=att)
                if len(af) != 1:
                    raise Exception("WTF dataset mark attribute field not found")
                else:
                    afield = af[0]
            else:
                raise Exception("WTF 2 global mark attribute fields with the same name")

            # if lookup, grab the value
            if afield.type == 'L':
                afv = AttributeFieldValue.objects.get(shortval=a['mark']['attributes'][att], field=afield)
                # returning any more than 1 value will raise an exception which is good
                maa = MarkAttribute.objects.create(mark=mark, field=afield, field_value=afv)
            # not a lookup, just store the value in the right field
            elif afield.type == 'F':
                ma = MarkAttribute.objects.create(mark=mark, field=afield, float_value=a['mark']['attributes'][att])
            elif afield.type == 'T':
                ma = MarkAttribute.objects.create(mark=mark, field=afield, text_value=a['mark']['attributes'][att])
            elif afield.type == 'I':
                ma = MarkAttribute.objects.create(mark=mark, field=afield, int_value=a['mark']['attributes'][att])
            elif afield.type == 'D':
                d = datetime.strptime(a['mark']['attributes'][att], '%d-%m-%Y %H:%M:%S')
                ma = MarkAttribute.objects.create(mark=mark, field=afield, time_value=d)
#        except Exception, e:
#            out['errors'] = []
#            out['errors'].append("Your request caused a processing exception: " + str(e))
#            return HttpResponse(json.dumps(out), content_type="application/json")


        # Successfully created an animal and no exception!
        
        #TODO change to final address
        out['redirect'] = "/portal/animal/view/"+str(animal.id)+"/?action=added_new_animal"
    else:
        out['errors'] = []
        out['errors'].append("Illegal request (not AJAX AND POST")
    return HttpResponse(json.dumps(out), content_type="application/json")

@login_required
@csrf_exempt
def json_edit_animal(request, aid=None):
    context = RequestContext(request)
    context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Edit Animal',
        }
    dataset_list = AnimalDataset.objects.order_by('name')
  
    animal = Animal.objects.get(pk=aid)
    context_dict['cur_animal'] = animal

    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk
  
    # shows user details
    uo = request.user
    context_dict['user']= uo
    up = UserProfile.objects.get(user=uo)
  
    # Prepare the context
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    # We've got a form with POST method so update CSRF token
    context_dict.update(csrf(request))
    # Check if the request is someone submitting a form (POST method)
    # If not POST then it's a GET so just display the (blank) form
    
    out = {}
    if request.is_ajax() and request.method == 'POST':
#        try:
        AnimalAttribute.objects.filter(animal=animal).delete();
        a = json.loads(request.body)
        uo = request.user
        # Grab current dataset
        cur_ds = AnimalDataset.objects.get(pk=request.session['cur_ds'])

        # FORM VALIDATION
        animal_gender = a['gender']
        animal_comments = a['comments']
        
        for att in a['attributes']:
 
            # grab the attribute
            af = AttributeField.objects.filter(is_global=True, table='A', name=att)
            if len(af) == 1:
                afield = af[0]
            elif len(af) == 0:
                af = AttributeField.objects.filter(is_global=False, dataset=cur_ds, table='A', name=att)
                if len(af) != 1:

                    raise Exception("WTF dataset animal attribute field not found")
                else:
                    afield = af[0]
            else:
                raise Exception("WTF 2 global animal attribute fields with the same name")
            
            relation = AnimalAttribute.objects.filter(animal=animal, field=afield)
            if len(relation) == 0:
                r1 = AnimalAttribute(animal=animal, field=afield)
            else:
                r1 = relation[0]
 
            # if lookup, grab the value
            if afield.type == 'L':
                afv = AttributeFieldValue.objects.get(shortval=a['attributes'][att], field=afield)

                # returning any more than 1 value will raise an exception which is good
                field=afield 
                field_value=afv
                r1.field_value = afv
            # not a lookup, just store the value in the right field
            elif afield.type == 'F':
                field=afield
                float_value=a['attributes'][att]
                r1.float_value = float_value
            elif afield.type == 'T':
                field=afield 
                text_value=a['attributes'][att]
                r1.text_value = text_value
            elif afield.type == 'I':
                field=afield
                int_value=a['attributes'][att]
                r1.int_value = int_value
            elif afield.type == 'D':
                d = datetime.strptime(a['attributes'][att], '%d-%m-%Y %H:%M:%S')
                field=afield
                time_value=d
                r1.time_value = time_value
            r1.save()
            

        now = datetime.now()

        animal.gender = animal_gender
        animal.comments =  animal_comments
        animal.modified_by = uo
        animal.date_modified = now
        animal.save()
        
        out['redirect'] = "http://albatross5.scriptforge.org:8020/portal/animal/view/"+str(animal.id)+"/?action=updated_animal"
    else:
        out['errors'] = []
        out['errors'].append("Illegal request (not AJAX AND POST")
    return HttpResponse(json.dumps(out), content_type="application/json")

    
    
@login_required
def edit(request, aid=None):
    context = RequestContext(request)
    context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Edit Animal',
        }
    dataset_list = AnimalDataset.objects.order_by('name')
    nolist_value = ''
    animal = Animal.objects.get(pk=aid)
    context_dict['cur_animal'] = animal

    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk
  
    # shows user details
    uo = request.user
    context_dict['user']= uo
    up = UserProfile.objects.get(user=uo)
  
    # Prepare the context
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    # We've got a form with POST method so update CSRF token
    context_dict.update(csrf(request))
    # Check if the request is someone submitting a form (POST method)
    # If not POST then it's a GET so just display the (blank) form
    

    # We need to lookup the mandatory fields for the dataset and globally
    min_animal_att_fields = []
    att_field_list = AttributeField.objects.filter(table='A', is_global=True, is_mandatory=True)
    for x in att_field_list:
        
        if x.type == 'L':
            fvals = AttributeFieldValue.objects.filter(field=x)
            # Lookup field values and put them in an array       
            for y in fvals:
                selected = AnimalAttribute.objects.filter(animal=animal,field=x, field_value = y)
                if len(selected) > 0:
                    y.selected = 1
                else:
                    y.selected = 0
        elif x.type == 'I':
            nolist_value = AnimalAttribute.objects.get(animal=animal, field=x)[0].int_value
            AnimalAttribute.objects.get(animal=animal, field=x)[0].int_value
        elif x.type == 'T':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].text_value
        elif x.type == 'F':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].float_value
        elif x.type == 'D':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].time_value
        x.vals = fvals   
        x.nolist_value = nolist_value   
        min_animal_att_fields.append(x)
        
    att_field_list = AttributeField.objects.filter(table='A', dataset=cur_ds, is_global=False,is_mandatory=True)
    for x in att_field_list:
        fvals = AttributeFieldValue.objects.filter(field=x)
        if x.type == 'L':
            # Lookup field values and put them in an array

            for y in fvals:
                selected = AnimalAttribute.objects.filter(animal=animal,field=x, field_value = y)
                if len(selected) > 0:
                    y.selected = 1
                else:
                    y.selected = 0
        elif x.type == 'I':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].int_value
        elif x.type == 'T':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].text_value
        elif x.type == 'F':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].float_value
        elif x.type == 'D':
            nolist_value = AnimalAttribute.objects.filter(animal=animal, field=x)[0].time_value
        x.vals = fvals
        x.nolist_value = nolist_value
        min_animal_att_fields.append(x)
    
    att_list = AnimalAttribute.objects.filter(animal=animal)
    for att in att_list :
        att_f = att.field 
        if (att_f.is_mandatory == True):
            continue
        else:         
            
            if (att_f.type == 'L') :
                fvals = AttributeFieldValue.objects.filter(field=att_f)
                for y in fvals:
                    selected = AnimalAttribute.objects.filter(animal=animal,field=att_f, field_value = y)
                    if len(selected) > 0:
                        y.selected = 1
                    else:
                        y.selected = 0
            elif att_f.type == 'I':
                nolist_value = AnimalAttribute.objects.filter(animal=animal, field=att_f)[0].int_value
            elif att_f.type == 'T':
                nolist_value = AnimalAttribute.objects.filter(animal=animal, field=att_f)[0].text_value
            elif att_f.type == 'F':
                nolist_value = AnimalAttribute.objects.filter(animal=animal, field=att_f)[0].float_value
            elif att_f.type == 'D':
                nolist_value = AnimalAttribute.objects.filter(animal=animal, field=att_f)[0].time_value
            
            att_f.vals = fvals
            att_f.nolist_value = nolist_value
        min_animal_att_fields.append(att_f)
    context_dict['min_animal_att_fields'] = min_animal_att_fields

    # Look up all fields that can be added
    all_animal_att_fields = []
    animal_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='A', dataset=cur_ds)
    for x in att_field_list:
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
        all_animal_att_fields.append(x)
    att_field_list = AttributeField.objects.filter(table='A', is_global=True).exclude(dataset=cur_ds)
    for x in att_field_list:
        if x.type == 'L':
            # Lookup field values and put them in an array
            fvals = AttributeFieldValue.objects.filter(field=x)
            x.vals = fvals
        all_animal_att_fields.append(x)
    context_dict['animal_att_field_values'] = animal_att_field_values
    context_dict['all_animal_att_fields'] = all_animal_att_fields
    
        # Render the form
    return render_to_response('editanimal.html', context_dict, context)


@login_required
def json_get_attribute_lookup_lists(request, tbl='A'):
    """
    This returns a JSON object of the attribute fields lookup values for forms
    builder to populate dropdowns
    """
    out = {}
    # get the global ones first
    fl = AttributeField.objects.filter(is_global=True, table=tbl)
    for f in fl:
        if f.type == 'L':
            vals = AttributeFieldValue.objects.filter(field=f)
            if len(vals) > 0:
                out[f.name] = []
                for v in vals:
                    out[f.name].append({'shortval': v.shortval, 'longval': v.longval})
    # get the ones just for the dataset next
    cur_ds = AnimalDataset.objects.get(pk=request.session['cur_ds'])
    fl = AttributeField.objects.filter(table=tbl, is_global=False, dataset=cur_ds)
    for f in fl:
        if f.type == 'L':
            vals = AttributeFieldValue.objects.filter(field=f)
            if len(vals) > 0:
                out[f.name] = []
                for v in vals:
                    out[f.name].append({'shortval': v.shortval, 'longval': v.longval})
    return HttpResponse(json.dumps(out), content_type="application/json")

@login_required
def view(request, aid=None):
    context = RequestContext(request)
    context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Edit Animal',
        }
    dataset_list = AnimalDataset.objects.order_by('name')

    animal = Animal.objects.get(pk=aid)
    context_dict['cur_animal'] = animal
      
    
    # shows user details
    uo = request.user
    context_dict['user']= uo
    up = UserProfile.objects.get(user=uo)

    # grabs current dataset (if not avalible set it)  
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    # prepare the Context
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list
    

    #find a list of marks belonging to that animal
    markList = []
    markList = Mark.objects.filter(animal=animal)

    context_dict['animal_marks'] = markList

    #find a list of logs belonging to that animal
    logList = Log.objects.filter(animal=animal)
    context_dict['animal_logs'] = logList

    return render_to_response('animal.html', context_dict, context)
    
