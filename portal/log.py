from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from portal.models import *
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.files.storage import default_storage
from django.shortcuts import redirect 


@login_required
def index(request):
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Keeping Track of Wildlife',
        }
  dataset_list = AnimalDataset.objects.order_by('name')


  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
 

  #Get log types
  log_type = Log.LOG_TYPES
  context_dict['log_type'] = log_type

    # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk
  # We've got a form with POST method so update CSRF token
  context_dict.update(csrf(request))
  # Check if the request is someone submitting a form (POST method)
  # If not POST then it's a GET so just display the (blank) form

  if request.method == 'POST':
    #validation
    log_date = datetime.strptime(request.POST['log_date'], '%d-%m-%Y %H:%M:%S')
    #log_date = request.POST.get('log_date', False)
    log_type = request.POST.get('log_type', False)
    mark = request.POST.get('mark', False)
    season = request.POST.get('season', False)
    if request.POST.get('sample_taken', 0):
      sample_taken = True
    else:
      sample_taken = False

    # validation
    if log_date is False:
      return HttpResponse("You didn't enter a log date!")
    if log_type is False:
      return HttpResponse("You didn't select a log type!")
    if mark is False:
      return HttpResponse("You didn't enter a mark type!")
    if season is False:
      return HttpResponse("You didn't select a season!")

    now = datetime.now()



    # add log to database
    log = Log.objects.create(animal=animal, log_date=now, log_type='R', season='', comments='New Animal Entered', sample_taken=False, date_created=now, created_by=uo, modified_by=uo)

    context_dict['info_msg'] = "Successfully created new animal with id " + str(animal.pk)
    # If all of the above worked, then we redirect back to dashboard
    return render_to_response('animal_success.html', context_dict, context)

  else:
    # Do the same for mark attributes
    # We need to lookup the mandatory fields for the dataset and globally
    min_log_att_fields = []
    att_field_list = AttributeField.objects.filter(table='L', is_mandatory=True)
    for x in att_field_list:
      min_log_att_fields.append(x)
      if x.type == 'L':
        # Lookup field values and put them in an array
        fvals = AttributeFieldValue.objects.filter(field=x)
        x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
      min_log_att_fields.append(x)
      if x.type == 'L':
        # Lookup field values and put them in an array
        fvals = AttributeFieldValue.objects.filter(field=x)
        x.vals = fvals
    context_dict['min_log_att_fields'] = min_log_att_fields

    # Look up all fields that can be added
    all_log_att_fields = []
    log_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='L', is_global=True)
    for x in att_field_list:
      all_log_att_fields.append(x)
      if x.type == 'L':
        # Lookup field values and put them in an array
        fvals = AttributeFieldValue.objects.filter(field=x)
        x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds)
    for x in att_field_list:
      all_log_att_fields.append(x)

    context_dict['log_att_field_values'] = log_att_field_values
    context_dict['all_log_att_fields'] = all_log_att_fields

    # Do the same for mark attributes
    # We need to lookup the mandatory fields for the dataset and globally
    min_breeding_att_fields = []
    att_field_list = AttributeField.objects.filter(table='B', is_mandatory=True)
    for x in att_field_list:
      min_breeding_att_fields.append(x)
      if x.type == 'L':
        # Lookup field values and put them in an array
        fvals = AttributeFieldValue.objects.filter(field=x)
        x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='B', dataset=cur_ds, is_mandatory=True)
    for x in att_field_list:
      min_breeding_att_fields.append(x)
      if x.type == 'L':
        # Lookup field values and put them in an array
        fvals = AttributeFieldValue.objects.filter(field=x)
        x.vals = fvals
    context_dict['min_breeding_att_fields'] = min_breeding_att_fields

    # Look up all fields that can be added
    all_breeding_att_fields = []
    breeding_att_field_values = {}
    att_field_list = AttributeField.objects.filter(table='B', is_global=True)
    for x in att_field_list:
      all_breeding_att_fields.append(x)
      if x.type == 'L':
        # Lookup field values and put them in an array
        fvals = AttributeFieldValue.objects.filter(field=x)
        x.vals = fvals
    att_field_list = AttributeField.objects.filter(table='B', dataset=cur_ds)
    for x in att_field_list:
      all_breeding_att_fields.append(x)

    context_dict['breeding_att_field_values'] = breeding_att_field_values
    context_dict['all_breeding_att_fields'] = all_breeding_att_fields

    return render_to_response('log.html', context_dict, context)

"""
############### Add log ##########################
"""

@login_required
def add(request, aid=None):
 # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = {# 'pagetitle': 'Threatened Species Portal',
       # 'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  if aid is not None:
    myanimal = Animal.objects.get(pk=aid)
    context_dict['cur_animal'] = myanimal

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  #indicate adding a new log
  context_dict['addlog'] = True

  #Get log types
  log_type = Log.LOG_TYPES
  context_dict['log_type'] = log_type

  # Get mark types
  mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
  context_dict['mark_types'] = mtypes['mark_types']


  # Do the same for mark attributes
  # We need to lookup the mandatory fields for the dataset and globally
  min_log_att_fields = []
  att_field_list = AttributeField.objects.filter(table='L', is_mandatory=True, is_global=True)
  for x in att_field_list:
    min_log_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds, is_mandatory=True)
  for x in att_field_list:
    min_log_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  context_dict['min_log_att_fields'] = min_log_att_fields

  # Look up all fields that can be added
  all_log_att_fields = []
  log_att_field_values = {}
  att_field_list = AttributeField.objects.filter(table='L', is_global=True)
  for x in att_field_list:
    all_log_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds)
  for x in att_field_list:
    all_log_att_fields.append(x)

  context_dict['log_att_field_values'] = log_att_field_values
  context_dict['all_log_att_fields'] = all_log_att_fields

  # Do the same for breeding attributes
  # We need to lookup the mandatory fields for the dataset and globally
  min_breeding_att_fields = []
  att_field_list = AttributeField.objects.filter(table='B', is_mandatory=True)
  for x in att_field_list:
    min_breeding_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='B', dataset=cur_ds, is_mandatory=True)
  for x in att_field_list:
    min_breeding_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  context_dict['min_breeding_att_fields'] = min_breeding_att_fields

  # Look up all fields that can be added
  all_breeding_att_fields = []
  breeding_att_field_values = {}
  att_field_list = AttributeField.objects.filter(table='B', is_global=True)
  for x in att_field_list:
    all_breeding_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='B', dataset=cur_ds)
  for x in att_field_list:
    all_breeding_att_fields.append(x)

  context_dict['breeding_att_field_values'] = breeding_att_field_values
  context_dict['all_breeding_att_fields'] = all_breeding_att_fields

  return render_to_response('log.html', context_dict, context)

"""
################## Log edit #######################
"""

@login_required
def edit(request, lid=None):
 # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  #indicate editing a log
  context_dict['addlog']= False

  # get log data
  log = Log.objects.get(pk=lid)
  context_dict['log']= log

  #get Breeding data if required
  if log.log_type =="B":
    breeding = Breeding.objects.get(log=log)
    context_dict['breeding']= breeding

    #if ther is a mate get mate mark details
    if breeding.mate:
      mateMark= Mark.objects.filter(animal=breeding.mate).filter(is_present=True).first()
      context_dict['mateMark'] = mateMark


    #if there is an offspring get offpsring mark details
    if breeding.offspring:
      offMark = Mark.objects.filter(animal=breeding.offspring).filter(is_present=True).first()
      context_dict['offMark'] = offMark

  #get file data if it exists
  try:
      file = File.objects.get(log=log)
  except File.DoesNotExist:
      file = None

  if file != None:
    context_dict['file']=file


  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  # Get log type
  log_types = Log.LOG_TYPES
  context_dict['log_type'] = log_types

  # Get mark types
  mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
  context_dict['mark_types'] = mtypes['mark_types']

  # Do the same for mark attributes
  # We need to lookup the mandatory fields for the dataset and globally
  min_log_att_fields = []
  att_field_list = AttributeField.objects.filter(table='L', is_mandatory=True, is_global=True)
  for x in att_field_list:
    min_log_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds, is_mandatory=True)
  for x in att_field_list:
    min_log_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  context_dict['min_log_att_fields'] = min_log_att_fields

  # Look up all fields that can be added
  all_log_att_fields = []
  log_att_field_values = {}
  att_field_list = AttributeField.objects.filter(table='L', is_global=True)
  for x in att_field_list:
    all_log_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='L', dataset=cur_ds)
  for x in att_field_list:
    all_log_att_fields.append(x)

  context_dict['log_att_field_values'] = log_att_field_values
  context_dict['all_log_att_fields'] = all_log_att_fields

  # Do the same for breeding attributes
  # We need to lookup the mandatory fields for the dataset and globally
  min_breeding_att_fields = []
  att_field_list = AttributeField.objects.filter(table='B', is_mandatory=True)
  for x in att_field_list:
    min_breeding_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='B', dataset=cur_ds, is_mandatory=True)
  for x in att_field_list:
    min_breeding_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  context_dict['min_breeding_att_fields'] = min_breeding_att_fields

  # Look up all fields that can be added
  all_breeding_att_fields = []
  breeding_att_field_values = {}
  att_field_list = AttributeField.objects.filter(table='B', is_global=True)
  for x in att_field_list:
    all_breeding_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='B', dataset=cur_ds)
  for x in att_field_list:
    all_breeding_att_fields.append(x)

  context_dict['breeding_att_field_values'] = breeding_att_field_values
  context_dict['all_breeding_att_fields'] = all_breeding_att_fields

  return render_to_response('log.html', context_dict, context)

"""
json_savelog() adds a new log
"""
@login_required
@csrf_exempt
def json_savelog(request):

  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
  'pagemsg': 'Add New Animal',
  }
  dataset_list = AnimalDataset.objects.order_by('name')

  # get user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  #Set time for creation/modification of record
  now = datetime.now()


  l = {}
  errors = {}
  if request.is_ajax() and request.method == 'POST':
    data = json.loads(request.body)
    l = data['log']
    errors['errors'] = []


    # validation
    #TO DO

    #validate base log info

    #date
    try:
      datetime.strptime(l['date'],'%Y-%m-%d %H:%M:%S')
    except ValueError:
      errors['errors'].append("Log: Date: invalid format. Must be YYYY-MM-DD HH-MM-SS")
    
    date = datetime.strptime(l['date'],'%Y-%m-%d %H:%M:%S')

    if date > now:
      errors['errors'].append("Log: Date: Date Can not be in the future")

    #Type
    if l['type'] =="":
      errors['errors'].append("Log: Type: invalid Log Type. Must be Resight, File or Breeding")



    #validate Resight data if needed
    if l['type'] != "F":
      #season
      if l['season']!="":
        if len(l['season'])!=4 or l['season'].isdigit() == False:
          errors['errors'].append("Resight: Season: invalid valid format. Must be YYYY")


      #sample
      if l['sample'] =="0" or l['sample'] =="1":

        if (l['sample'] == "0"):
          sample=False
        else:
          sample=True

      else:
        errors['errors'].append("Resight: Sample Taken: Value Invalid")


      #comments


      #validate Resight Attributes

    #Validate Brreding data if needed
    if l['type'] == "B":
      b = data['log']['breeding']

      #validate Breeding Season
      if b['season']!="":
        if len(b['season'])!=4 or b['season'].isdigit() == False:
          errors['errors'].append("Breeding Season: invalid valid format. Must be YYYY")


      #validate Breeding Comments
      #b['comments']

      #validate and find mate
      if b['mate_m_type'] != "" and b['mate_m_id'] != "":
        #Find mate
        try:
          mate_mark = Mark.objects.get(mark_type=b['mate_m_type'], mark_number=b['mate_m_id'])      
	  #get mate object
	  mate = mate_mark.animal
        #if mate cant be found error**(needs work)
        except Mark.DoesNotExist:
          errors['errors'].append("Can not find Mate: check mark type and number")


      #validate and find offspring
      if b['off_m_type'] != "" and b['off_m_id'] != "":
        #Find offspring
        try:
          off_mark = Mark.objects.get(mark_type=b['off_m_type'], mark_number=b['off_m_id'])
	  #get Offspring Object
	  offspring = off_mark.animal
        #if offspring cant be found error**(needs work)
        except Mark.DoesNotExist:
          errors['errors'].append("Can not find Offspring: check mark type and number")

      #validate Breeding Attributes

    #validate File
    if 'file' in data['log']:
      f = l['file']
    
    #get animal object
    try:
      animal = Animal.objects.get(pk=l['id'])
    except Animal.DoesNotExist:
      errors['errors'].append("Can not find Animal")
      

    #if any errors display to user and do not create log
    if errors['errors']:
      return HttpResponse(json.dumps(errors), content_type="application/json")

    # get basic log values
    date=l['date']
    type=l['type']


    # create basic log
    log=Log.objects.create(animal=animal, log_date=date, log_type=type, date_created=now, created_by=uo, modified_by=uo, date_modified=now)


    # Create Resight log if log type is not file
    if l['type'] != "F":
      season=l['season']
      comments=l['comments']
      if (l['sample'] == "0"):
        sample=False
      else:
        sample=True

      log.season = season
      log.comments = comments
      log.sample_taken = sample
      log.save()

      # add resight log attributes to database
      for att in l['attributes']:
	      # grab the attribute
	      af = AttributeField.objects.filter(is_global=True, table='L', name=att)
	      if len(af) == 1:
		      afield = af[0]
	      elif len(af) == 0:
		      af = AttributeField.objects.filter(is_global=False, table='L', dataset=cur_ds, name=att)
		      if len(af) != 1:
			      raise Exception("WTF dataset Log attribute field not found")
		      else:
			      afield = af[0]
	      else:
		      raise Exception("WTF 2 global Log attribute fields with the same name")

	      # if lookup, grab the value
	      if afield.type == 'L':
		      afv = AttributeFieldValue.objects.get(shortval=l['attributes'][att], field=afield)
		      # returning any more than 1 value will raise an exception which is good
		      aa = LogAttribute.objects.create(Log=log, field=afield, field_value=afv)
	      # not a lookup, just store the value in the right field
	      elif afield.type == 'F':
		      aa = LogAttribute.objects.create(Log=log, field=afield, float_value=l['attributes'][att])
	      elif afield.type == 'T':
		      aa = LogAttribute.objects.create(Log=log, field=afield, text_value=l['attributes'][att])
	      elif afield.type == 'I':
		      aa = LogAttribute.objects.create(Log=log, field=afield, int_value=l['attributes'][att])
	      elif afield.type == 'D':
		      d = datetime.strptime(l['attributes'][att], '%d-%m-%Y %H:%M:%S')
		      aa = LogAttribute.objects.create(Log=log, field=afield, time_value=d)

    # If Breeding Log
    if l['type'] == "B":


      # get breeding values
      season = b['season']
      comments = b['comments']


      #add basic breeding to the database
      breeding = Breeding.objects.create(log= log, season= season, comments = comments,  date_created=now, created_by=uo, modified_by=uo, date_modified=now)

      #if Mate mark detials were entered
      if b['mate_m_type'] != "" and b['mate_m_id']!= "":
        #update breeding with validated mate
        breeding.mate = mate
        breeding.save()


      #if offspring mark details were entered
      if b['off_m_type'] != "" and b['off_m_id'] != "":
        #update breeding with validated offspring
        breeding.offspring = offspring
        breeding.save()

      # add breeding attributes to database
      for att in b['attributes']:
	# grab the attribute
	af = AttributeField.objects.filter(is_global=True, table='B', name=att)
	if len(af) == 1:
		afield = af[0]
	elif len(af) == 0:
		af = AttributeField.objects.filter(is_global=False, table='B', dataset=cur_ds, name=att)
		if len(af) != 1:
			raise Exception("WTF dataset Breeding attribute field not found")
		else:
			afield = af[0]
	else:
		raise Exception("WTF 2 global Breeding attribute fields with the same name")

	# if lookup, grab the value
	if afield.type == 'L':
		afv = AttributeFieldValue.objects.get(shortval=b['attributes'][att], field=afield)
		# returning any more than 1 value will raise an exception which is good
		aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, field_value=afv)
	# not a lookup, just store the value in the right field
	elif afield.type == 'F':
		aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, float_value=b['attributes'][att])
	elif afield.type == 'T':
		aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, text_value=b['attributes'][att])
	elif afield.type == 'I':
		aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, int_value=b['attributes'][att])
	elif afield.type == 'D':
		d = datetime.strptime(b['attributes'][att], '%d-%m-%Y %H:%M:%S')
		aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, time_value=d)

    #create file if needed
    if 'file' in data['log']:
      description = f['description']
      
      file = File.objects.create(log= log, description = description, upload_time= now, date_created= now, created_by = uo, modified_by = uo, date_modified= now)
      
      upload = {}
      upload['file'] = log.id
      return HttpResponse(json.dumps(upload), content_type="application/json")
      
      
    sucess = {}
    #TODO add url varible for sucess
    #change this redirect address if the URL for the application changes
    sucess['redirect'] = "/portal/animal/view/"+str(log.animal.id)+"/?action=added_new_log"
    
    return HttpResponse(json.dumps(sucess), content_type="application/json")
  return HttpResponse(json.dumps(l), content_type="application/json")

"""
json_editlog() updates an existing log
"""
@login_required
@csrf_exempt
def json_editlog(request):

  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
  'pagemsg': 'edit log',
  }
  dataset_list = AnimalDataset.objects.order_by('name')

  # get user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  #Set time for creation/modification of record
  now = datetime.now()


  l = {}
  errors = {}
  if request.is_ajax() and request.method == 'POST':
    data = json.loads(request.body)
    l = data['log']
    errors['errors'] = []


    # validation
    #TO DO

    #validate base log info

    #date
    try:
      datetime.strptime(l['date'],'%Y-%m-%d %H:%M:%S')
    except ValueError:
      errors['errors'].append("Log: Date: invalid format. Must be YYYY-MM-DD HH-MM-SS")
    
    date = datetime.strptime(l['date'],'%Y-%m-%d %H:%M:%S')

    if date > now:
      errors['errors'].append("Log: Date: Date Can not be in the future")


    #Type
    if l['type'] =="":
      errors['errors'].append("Log: Type: invalid Log Type. Must be Resight, File or Breeding")



    #validate Resight data if needed
    if l['type'] != "F":
      #season
      if l['season']!="":
        if len(l['season'])!=4 or l['season'].isdigit() == False:
          errors['errors'].append("Resight: Season: invalid valid format. Must be YYYY")


      #sample
      if l['sample'] =="0" or l['sample'] =="1":

        if (l['sample'] == "0"):
          sample=False
        else:
          sample=True

      else:
        errors['errors'].append("Resight: Sample Taken: Value Invalid")


      #comments


      #validate Resight Attributes

    #Validate Brreding data if needed
    if l['type'] == "B":
      b = data['log']['breeding']

      #validate Breeding Season
      if b['season']!="":
        if len(b['season'])!=4 or b['season'].isdigit() == False:
          errors['errors'].append("Breeding Season: invalid valid format. Must be YYYY")


      #validate Breeding Comments
      #b['comments']

      #validate and find mate
      if b['mate_m_type'] != "" and b['mate_m_id'] != "":
        #Find mate
        try:
          mate_mark = Mark.objects.get(mark_type=b['mate_m_type'], mark_number=b['mate_m_id'])
          #get mate object
	  mate = mate_mark.animal
        #if mate cant be found error**(needs work)
        except Mark.DoesNotExist:
          errors['errors'].append("Can not find Mate: check mark type and number")

      #validate and find offspring
      if b['off_m_type'] != "" and b['off_m_id'] != "":
        #Find offspring
        try:
          off_mark = Mark.objects.get(mark_type=b['off_m_type'], mark_number=b['off_m_id'])
          #get Offspring Object
	  offspring = off_mark.animal

        #if offspring cant be found error**(needs work)
        except Mark.DoesNotExist:
          errors['errors'].append("Can not find Offspring: check mark type and number")

    # validate Breeding Attributes

    # validate File

    #get log object
    try:
      log = Log.objects.get(pk=l['id'])
    except Log.DoesNotExist:
      errors['errors'].append("Can not find Log")
      
    #get breeding object if it exists
    if log.log_type =="B": 
    
      try:
	breeding = Breeding.objects.get(log=log)
      except Breeding.DoesNotExist:
	errors['errors'].append("Can not find Breeding Data")
	

    if log.log_type =="F":
	   
      try:
	file = File.objects.get(log=log)
      except File.DoesNotExist:
	errors['errors'].append("Can not find File data")
    else:
      try:
	file = File.objects.get(log=log)
      except File.DoesNotExist:
	file = None      
  
    # if any errors display to user and do not create log
    if errors['errors']:
      return HttpResponse(json.dumps(errors), content_type="application/json")
    
    # get basic log values
    type=l['type']

    # update log here as needed
    # check log type
    if type == log.log_type:

      # Resight
      if log.log_type == "R":
        #update Resight log
        
        if log.log_date != l['date']:
	  log.log_date = l['date']
	  
	if log.season != l['season']:
	  log.season = l['season']
	  
	if log.comments != l['comments']:  
	  log.comments = l['comments']
	  
	if log.sample_taken == True and sample == False or log.sample_taken == False and sample == True:
	  log.sample_taken = sample
                
        log.modified_by = uo
	log.date_modified = now
	log.save()
        #update Resight Attributes

        #update or create  file if required
        if file != None:
	  f = l['file']
	  
	  if file.description != f['description']:
	    file.description = f['description']
	    file.modified_by= uo
	    file.date_modified = now
	    file.save()
	    
	if 'file' in data['log'] and file == None:
          description = f['description']
      
          file = File.objects.create(log= log, description = description, upload_time= now, date_created= now, created_by = uo, modified_by = uo, date_modified= now)
	
	sucess = {}
	#TODO add url varible for sucess
	#change this redirect address if the URL for the application changes
	sucess['redirect'] = "/portal/animal/view/"+str(log.animal.id)+"/?action=updated_log"
	return HttpResponse(json.dumps(sucess), content_type="application/json")

      #Breeding
      if log.log_type == "B":
        #update Resight Log
        if log.log_date != l['date']:
	  log.log_date = l['date']
	  
	if log.season != l['season']:
	  log.season = l['season']
	  
	if log.comments != l['comments']:  
	  log.comments = l['comments']
	  
	if log.sample_taken == True and sample == False or log.sample_taken == False and sample == True:
	  log.sample_taken = sample
                
        log.modified_by = uo
	log.date_modified = now
	log.save()
        #update Resight Attributes

        #update Breeding Log
	if breeding.season != b['season']:      
	  breeding.season= b['season']
	  
	if breeding.comments != b['comments']:  
	  breeding.comments = b['comments']  
	
	#if Mate mark detials were entered
	if b['mate_m_type'] != "" and b['mate_m_id']!= "":
	  #update breeding with validated mate
	  if breeding.mate != mate:
	    breeding.mate = mate
	    
	#if offspring mark details were entered
	if b['off_m_type'] != "" and b['off_m_id'] != "":
	  #update breeding with validated offspring
	  if breeding.offspring != offspring:
	    breeding.offspring = offspring
	  	  
	breeding.modified_by=uo
	breeding.date_modified=now
	breeding.save()
        #update Breeding Attributes

        #update or create file if required
        if file != None:
	  f = l['file']
	  
	  if file.description != f['description']:
	    file.description = f['description']
	    file.modified_by= uo
	    file.date_modified = now
	    file.save()
	    
	if 'file' in data['log'] and file == None:
          description = f['description']
      
          file = File.objects.create(log= log, description = description, upload_time= now, date_created= now, created_by = uo, modified_by = uo, date_modified= now)
          
	sucess = {}
	#TODO add url varible for sucess
	#change this redirect address if the URL for the application changes
	sucess['redirect'] = "/portal/animal/view/"+str(log.animal.id)+"/?action=updated_log"
	return HttpResponse(json.dumps(sucess), content_type="application/json")

      #File
      if log.log_type == "F":
        #update basic log
        if log.log_date != l['date']:
	  log.log_date = l['date']
	  
	log.modified_by = uo
	log.date_modified = now
	log.save()
        #update File
        if file != None:
	  f = l['file']
	  
	  if file.description != f['description']:
	    file.description = f['description']
	    file.modified_by= uo
	    file.date_modified = now
	    file.save()
	    
	if 'file' in data['log'] and file == None:
          description = f['description']
      
          file = File.objects.create(log= log, description = description, upload_time= now, date_created= now, created_by = uo, modified_by = uo, date_modified= now)
          
	sucess = {}
	#TODO add url varible for sucess
	#change this redirect address if the URL for the application changes
	sucess['redirect'] = "/portal/animal/view/"+str(log.animal.id)+"/?action=updated_log"
	return HttpResponse(json.dumps(sucess), content_type="application/json")
    else:

      #Resight to Breeding (add breeding info)
      if type == "B" and log.log_type =="R":

        #update Resight log
        log.log_type = l['type']
        
        if log.log_date != l['date']:
	  log.log_date = l['date']
	  
	if log.season != l['season']:
	  log.season = l['season']
	  
	if log.comments != l['comments']:  
	  log.comments = l['comments']
	  
	if log.sample_taken == True and sample == False or log.sample_taken == False and sample == True:
	  log.sample_taken = sample
                
        log.modified_by = uo
	log.date_modified = now
	log.save()
	
        #update Resight Attributes

        #Create Breeding Object
	
	# get breeding values
	season = b['season']
	comments = b['comments']


	#add basic breeding to the database
	breeding = Breeding.objects.create(log= log, season= season, comments = comments,  date_created=now, created_by=uo, modified_by=uo, date_modified=now)

	#if Mate mark detials were entered
	if b['mate_m_type'] != "" and b['mate_m_id']!= "":
	  #update breeding with validated mate
	  breeding.mate = mate
	  breeding.save()


	#if offspring mark details were entered
	if b['off_m_type'] != "" and b['off_m_id'] != "":
	  #update breeding with validated offspring
	  breeding.offspring = offspring
	  breeding.save()
	
	
        #Create Breeding Attributes
        for att in b['attributes']:
	  # grab the attribute
	  af = AttributeField.objects.filter(is_global=True, table='B', name=att)
	  if len(af) == 1:
		  afield = af[0]
	  elif len(af) == 0:
		  af = AttributeField.objects.filter(is_global=False, table='B', dataset=cur_ds, name=att)
		  if len(af) != 1:
			  raise Exception("WTF dataset Breeding attribute field not found")
		  else:
			  afield = af[0]
	  else:
		  raise Exception("WTF 2 global Breeding attribute fields with the same name")

	  # if lookup, grab the value
	  if afield.type == 'L':
		  afv = AttributeFieldValue.objects.get(shortval=b['attributes'][att], field=afield)
		  # returning any more than 1 value will raise an exception which is good
		  aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, field_value=afv)
	  # not a lookup, just store the value in the right field
	  elif afield.type == 'F':
		  aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, float_value=b['attributes'][att])
	  elif afield.type == 'T':
		  aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, text_value=b['attributes'][att])
	  elif afield.type == 'I':
		  aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, int_value=b['attributes'][att])
	  elif afield.type == 'D':
		  d = datetime.strptime(b['attributes'][att], '%d-%m-%Y %H:%M:%S')
		  aa = BreedingAttribute.objects.create(Breeding=breeding, field=afield, time_value=d)

        #update or create file if required
        if file != None:
	  f = l['file']
	  
	  if file.description != f['description']:
	    file.description = f['description']
	    file.modified_by= uo
	    file.date_modified = now
	    file.save()
	    
	if 'file' in data['log'] and file == None:
          description = f['description']
      
          file = File.objects.create(log= log, description = description, upload_time= now, date_created= now, created_by = uo, modified_by = uo, date_modified= now)
	
	sucess = {}
	#TODO add url varible for sucess
	#change this redirect address if the URL for the application changes
	sucess['redirect'] = "/portal/animal/view/"+str(log.animal.id)+"/?action=updated_log"
	return HttpResponse(json.dumps(sucess), content_type="application/json")
      
      # Breeding to Resight (delete breeding info)
      #if type == "R" and log.log_type =="B":

        #update Resight log

        #update Resight Attributes

        #Delete breeding object

        #delete Breeding Attributes

        #update or create file if required
        

  return HttpResponse(json.dumps(l), content_type="application/json")
  
@login_required
def json_get_attribute_lookup_lists(request, tbl=None):
    """
    This returns a JSON object of the attribute fields lookup values for forms
    builder to populate dropdowns
    """
    out = {}
    # get the global ones first
    fl = AttributeField.objects.filter(is_global=True, table=tbl)
    for f in fl:
        if f.type == 'L':
            vals = AttributeFieldValue.objects.filter(field=f)
            if len(vals) > 0:
                out[f.name] = []
                for v in vals:
                    out[f.name].append({'shortval': v.shortval, 'longval': v.longval})
    # get the ones just for the dataset next
    cur_ds = AnimalDataset.objects.get(pk=request.session['cur_ds'])
    fl = AttributeField.objects.filter(table=tbl, is_global=False, dataset=cur_ds)
    for f in fl:
        if f.type == 'L':
            vals = AttributeFieldValue.objects.filter(field=f)
            if len(vals) > 0:
                out[f.name] = []
                for v in vals:
                    out[f.name].append({'shortval': v.shortval, 'longval': v.longval})
    return HttpResponse(json.dumps(out), content_type="application/json")
 
@login_required
@csrf_exempt
def upload(request, lid=None):
 # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = {# 'pagetitle': 'Threatened Species Portal',
       # 'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  
  #Set time for creation/modification of record
  now = datetime.now()
  
  # get log data
  if lid is not None:
    log = Log.objects.get(pk=lid)
    context_dict['log']= log
  
  #get file data if it exists
  try:
      file = File.objects.get(log=log)
  except File.DoesNotExist:
      file = None

  if file != None:
    context_dict['file']=file
    
  if request.method == 'POST':
    #do stuff with file here
    if 'file' in request.FILES:
      upload = request.FILES['file']
      
      
      
      file.file 
      file.type = upload['type']
      file.sha1sum = 1
      file.upload_time = now
      file.save()
        
    #need to get view to display update or added log message
    return redirect('view', aid=log.animal.id) 
    return render_to_response('log.html', context_dict, context)
  
  return render_to_response('log.html', context_dict, context)
  
