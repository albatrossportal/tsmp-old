from django.contrib import admin
from models import *

admin.site.register(UserProfile)
admin.site.register(AnimalDataset)
admin.site.register(Animal)
admin.site.register(Mark)
admin.site.register(Log)
admin.site.register(Breeding)
admin.site.register(File)
admin.site.register(AttributeField)

admin.site.register(AttributeFieldValue)
admin.site.register(MarkAttribute)
admin.site.register(LogAttribute)
admin.site.register(BreedingAttribute)
admin.site.register(AnimalAttribute)

admin.site.register(Queries)
admin.site.register(Backup)
