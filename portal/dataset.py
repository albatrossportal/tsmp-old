from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.models import User, Group
from portal.models import *
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from datetime import datetime

@login_required
def index(request):
  # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  return render_to_response('dataset_list.html', context_dict, context)

# This shows the dataset detail form
@login_required
def ajax_add_form(request):
  context = RequestContext(request)
  context_dict = {}
  context_dict['adddataset'] = True
  users = User.objects.all()
  context_dict['userlist'] = users
  if request.user.is_superuser:
    return render_to_response('dataset_detail.html', context_dict, context)
  else:
    return render_to_response('noperms.html', context_dict, context)

# This shows the dataset detail form for a specific dataset
@login_required
def ajax_edit_form(request, did=None):
  context = RequestContext(request)
  context_dict = {}
  context_dict['adddataset'] = True

  if request.user.is_superuser:
    if did is None:
      return HttpResponse("Invalid datasetid.")
    ds = AnimalDataset.objects.get(pk=did)
    users = User.objects.all()
    context_dict['userlist'] = users
    context_dict['ds'] = ds
    context_dict['adddataset'] = False
    return render_to_response('dataset_detail.html', context_dict, context)
  else:
    return render_to_response('noperms.html', context_dict, context)

# This changes the current dataset (session value)
@login_required
def change_current(request, did=None):
  if did is None:
    return HttpResponse("Invalid datasetid.")
  ds = AnimalDataset.objects.get(pk=did)
  if did is None:
    return HttpResponse("Invalid datasetid.")
  else:
    request.session['cur_ds'] = ds.pk
    return HttpResponseRedirect('/portal')

# this saves form data
@login_required
def new_dataset(request):
  # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  context_dict.update(csrf(request))

  if not request.user.is_superuser:
    return HttpResponse("Not Superuser")

  if request.method == "POST":
   id = request.POST['id'] 
   name = request.POST['name']
   description = request.POST['description']
   owner = User.objects.get(pk=request.POST['owner'])
   now = datetime.now()
  
   if request.POST.get("new"):
    
    dataset = AnimalDataset.objects.create(name=name, owner= owner, description =description, date_created =now, created_by =uo, date_modified= now, modified_by =uo)
    
    # creating group
    g = Group(name=str(dataset.id)+'_can_edit')
    g.save()
    g = Group(name=str(dataset.id)+'_can_admin')
    g.save()
    g = Group(name=str(dataset.id)+'_can_read')
    g.save()
    
    AttributeField.objects.create(dataset=dataset, table='A', name='Species Common Name', type='L', description='The common name for the species', is_mandatory=True, is_global=False)
    AttributeField.objects.create(dataset=dataset, table='A', name='Species Scientific Name', type='L', description='The scientific/latin name for the species', is_mandatory=True, is_global=False)
    AttributeField.objects.create(dataset=dataset, table='L', name='Location', type='T', description='The name of the place where the animal was sighted', is_mandatory=True, is_global=False)
    
    context_dict['info_msg'] = "Successfully created dataset" 
    
   else:
    
    ds = AnimalDataset.objects.get(id =id)   

    ds.name = name
    ds.owner = owner
    ds.description = description
    ds.date_modified = now
    ds.modified_by = uo
    ds.save()

   context_dict['info_msg'] = "Successfully updated dataset"
   return render_to_response('dataset_list.html', context_dict, context)
  else:
    return HttpResponse("Nothing POSTed")
