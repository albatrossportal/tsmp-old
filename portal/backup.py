from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from portal.models import *
import albatross.settings as settings
import os.path
import json

@login_required
def index(request):
  context = RequestContext(request)
  context_dict = { 'pagetitle':	'Threatened Species Portal',
        'pagemsg': 'Keeping Track of Wildlife',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  # Determine if user is admin, if not redirect to no_perms page
  if request.user.is_superuser:
    # Display User List
    backup_list = Backup.objects.all()
    bl = []
    for b in backup_list:
        name = b.backupfile.name.replace(settings.MEDIA_ROOT+'/backups/', '')
        u = b.backupfile.url.replace(settings.MEDIA_ROOT, '/media')
        #TODO Security - need a view to serve up the zip file instead of directly from /media as it is not secure
        bl.append({'created': b.created_time, 'url': u, 'name': name})
    context_dict['backup_list'] = bl
    return render_to_response('backup.html', context_dict, context)
  else:
    # No Permissions Page
    return render_to_response('noperms.html', context_dict, context)

def json_request_backup(request):
  if not os.path.isfile('/tmp/backupreq') and not os.path.isfile('/tmp/backupstate'):
    with open('/tmp/backupreq', 'w') as f:
      f.write("Backup")
    out = {'success': 'Request successful'}
  else:
    out = {'error': 'Backup already requested and/or in progress'}
  return HttpResponse(json.dumps(out), content_type='application/json')
