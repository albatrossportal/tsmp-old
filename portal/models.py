from django.db import models
from django.contrib.auth.models import User
import tsmp.settings as settings
import os; 


def content_file_name(self, filename):
  
    if not os.path.exists(os.path.join(settings.MEDIA_ROOT,"log")):
     os.makedirs(os.path.join(settings.MEDIA_ROOT,"log"))
    
    if not os.path.exists(os.path.join(settings.MEDIA_ROOT,"log", str(self.log.id))):
      os.mkdir(os.path.join(settings.MEDIA_ROOT, "log", str(self.log.id)))
    
    return "media/log/%d/%s" % (self.log.id, filename)

def backup_file_name(self, filename):
    return "backups/%d/%s" % (self.log.id, filename)


class AnimalDataset (models.Model):
    name = models.CharField(max_length=128, unique=True)
    owner = models.ForeignKey(User)
    description = models.TextField(blank=True)
    date_created = models.DateTimeField()
    created_by = models.ForeignKey(User, related_name="dataset_created_user")
    date_modified = models.DateTimeField()
    modified_by = models.ForeignKey(User, related_name="dataset_modified_user")

    class Meta:
        verbose_name = "Dataset"
        verbose_name_plural = "Datasets"

    def __str__(self):
        return self.name


class Animal(models.Model):
    MALE = 'M'
    FEMALE = 'F'
    UNKNOWN = 'U'
    GENDER_TYPES = (
        (UNKNOWN, 'Unknown'),
        (MALE, 'Male'), 
        (FEMALE, 'Female'),
    )
    dataset = models.ForeignKey(AnimalDataset)
    gender = models.CharField(
        max_length=2,
        choices=GENDER_TYPES,
        default=UNKNOWN, db_index=True)
    comments = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.ForeignKey(User, related_name="animal_created_user")
    date_modified = models.DateTimeField()
    modified_by = models.ForeignKey(User, related_name="animal_modified_user")

    class Meta:
        verbose_name = "Animal"
        verbose_name_plural = "Animals"

    def __str__(self):
        return '%s %s' % ("Animal no:", self.pk)

    @staticmethod
    def get_fieldlist():
        fl = []
        for field in Animal._meta.fields:
            if field.name != 'id' and field.name != 'dataset':
                fl.append(
                    {
                        'name': field.name,
                        'verbose_name': field.verbose_name,
                        'type': field.get_internal_type()
                    })
            if field.name == 'id':
                fl.append(
                    {
                        'name': 'id',
                        'verbose_name': 'Animal',
                        'type': 'AutoField'
                    })
        return fl


class Log(models.Model):
    RESIGHT = 'R'
    FILE = 'F'
    BREEDING = 'B'
    LOG_TYPES = (
        (RESIGHT, 'Resight'),
        (FILE, 'File'),
        (BREEDING, 'Breeding')
    )
    animal = models.ForeignKey(Animal)
    log_date = models.DateTimeField(db_index=True)
    log_type = models.CharField(
        max_length=2,
        choices=LOG_TYPES,
        default=RESIGHT, db_index=True)
    season = models.CharField(max_length=10, blank=True, db_index=True)
    comments = models.TextField(blank=True, null=True)
    sample_taken = models.BooleanField(default=False, db_index=True)
    date_created = models.DateTimeField()
    created_by = models.ForeignKey(User, related_name="log_created_user")
    date_modified = models.DateTimeField()
    modified_by = models.ForeignKey(User, related_name="log_modified_user")

    @staticmethod
    def get_fieldlist():
        fl = []
        for field in Log._meta.fields:
            if field.name != 'id' and field.name != 'animal':
                fl.append(
                    {
                        'name': field.name,
                        'verbose_name': field.verbose_name,
                        'type': field.get_internal_type()
                    })
        return fl

    def __str__(self):
        return '%s %s' % ("Log no:", self.pk)


class Mark(models.Model):
    class Meta:
        verbose_name = "Mark"
        verbose_name_plural = "Marks"

    mark_number = models.CharField(max_length=128, db_index=True)
    mark_type = models.CharField(max_length=128, db_index=True)
    animal = models.ForeignKey(Animal)
    is_present = models.BooleanField(default=False, db_index=True)
    date_attached = models.DateTimeField(db_index=True)
    comments = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.ForeignKey(User, related_name="mark_created_user")
    date_modified = models.DateTimeField()
    modified_by = models.ForeignKey(User, related_name="mark_modified_user")

    def __str__(self):
        return '%s %s' % ("Mark no:", self.pk)

    @staticmethod
    def get_fieldlist():
        fl = []
        for field in Mark._meta.fields:
            if field.name != 'id' and field.name != 'animal':
                fl.append(
                    {
                        'name': field.name,
                        'verbose_name': field.verbose_name,
                        'type': field.get_internal_type()
                    })
        return fl

    @staticmethod
    def get_mark_types(dataset=None):
        """
        Gets a list of all mark types in a specific dataset
        """
        out = {}
        if dataset is None:
            out['error'] = 'No list of marks could be retrieved'
        else:
            try:
                t = Mark.objects.values('mark_type').distinct()
                out['mark_types'] = []
                for m in t:
                    out['mark_types'].append(m['mark_type'])
            except Mark.DoesNotExist:
                out['error'] = 'No list of marks could be retrieved'
        return out

    @staticmethod
    def get_mark_numbers(dataset=None):
        """
        Gets a list of all mark numbers in a specific dataset
        """
        out = {}
        if dataset is None:
            out['error'] = 'No list of marks could be retrieved'
        else:
            try:
                t = Mark.objects.values('mark_number').distinct()
                out['mark_numbers'] = []
                for m in t:
                    out['mark_numbers'].append(m['mark_number'])
            except Mark.DoesNotExist:
                out['error'] = 'No list of marks could be retrieved'
        return out


class Breeding(models.Model):
    log = models.ForeignKey(Log)
    mate = models.ForeignKey(Animal, related_name="breeding_mate", null=True)
    offspring = models.ForeignKey(Animal,
        related_name="breeding_offspring",
        null=True)
    season = models.CharField(max_length=10, blank=True, db_index=True)
    comments = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.ForeignKey(User, related_name="breeding_created_user")
    date_modified = models.DateTimeField()
    modified_by = models.ForeignKey(User, related_name="breeding_modified_user")

    class Meta:
        verbose_name = "Breeding"
        verbose_name_plural = "Breeding"

    def __str__(self):
        return '%s %s' % ("Breeding Attempt no:", self.pk)

    @staticmethod
    def get_fieldlist():
        fl = []
        for field in Breeding._meta.fields:
            if field.name != 'id' and field.name != 'log':
                fl.append(
                    {
                        'name': field.name,
                        'verbose_name': field.verbose_name,
                        'type': field.get_internal_type()
                    })
        return fl


class File(models.Model):
    log = models.ForeignKey(Log)
    file = models.FileField(upload_to=content_file_name)
    type = models.CharField(max_length=128)
    sha1sum = models.TextField()
    description = models.TextField(blank=True, null=True)
    upload_time = models.DateTimeField()
    date_created = models.DateTimeField()
    created_by = models.ForeignKey(User, related_name="files_created_user")
    date_modified = models.DateTimeField()
    modified_by = models.ForeignKey(User, related_name="files_modified_user")

    class Meta:
        verbose_name = "File"
        verbose_name_plural = "Files"

    def __str__(self):
        return self.sha1sum

class AttributeField (models.Model):
    LOOKUP = 'L'
    INTEGER = 'I'
    FLOAT = 'F'
    TEXT = 'T'
    DATE = 'D'
    ATTRIBUTE_TYPES = (
        (LOOKUP, 'Lookup'),
        (INTEGER, 'Integer'),
        (FLOAT, 'Float'),
        (TEXT, 'Text'),
        (DATE, 'Date/Time'),
    )
    ANIMAL = 'A'
    MARK = 'M'
    BREEDING = 'B'
    LOG = 'L'
    TABLE_LIST = (
        (ANIMAL, 'Animal'),
        (MARK, 'Mark'),
        (LOG, 'Log'),
        (BREEDING, 'Breeding'),
    )
    dataset = models.ForeignKey(AnimalDataset)
    table = models.CharField(max_length=2, choices=TABLE_LIST, default=ANIMAL)
    name = models.CharField(max_length=128)
    type = models.CharField(max_length=2, choices=ATTRIBUTE_TYPES, default=TEXT)
    description = models.TextField(blank=True)
    is_mandatory = models.BooleanField(default=False)
    is_global = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    @staticmethod
    def get_fieldlist(tbl, ds):
        out = []
        # grab global attributes
        gla = AttributeField.objects.filter(is_global=True, table=tbl)
        for x in gla:
            out.append({'name': x.name, 'type': x.type})
        # grab dataset attributes
        try:
            d = AnimalDataset.objects.get(pk=ds)
        except AnimalDataset.DoesNotExist:
            return out
        dsa = AttributeField.objects.filter(
            is_global=False,
            dataset=d,
            table=tbl)
        for x in dsa:
            out.append({'name': x.name, 'type': x.type})
        return out


class AttributeFieldValue(models.Model):
    field = models.ForeignKey(AttributeField)
    shortval = models.CharField(max_length=64)
    longval = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return self.shortval


class MarkAttribute(models.Model):
    mark = models.ForeignKey(Mark)
    field = models.ForeignKey(AttributeField)
    field_value = models.ForeignKey(AttributeFieldValue, null=True, blank=True)
    text_value = models.CharField(max_length=256, blank=True, null=True, db_index=True)
    int_value = models.BigIntegerField(blank=True, null=True, db_index=True)
    float_value = models.FloatField(blank=True, null=True, db_index=True)
    time_value = models.DateTimeField(blank=True, null=True, db_index=True)

    class Meta:
        verbose_name = "Mark Attribute"
        verbose_name_plural = "Mark Attributes"

    def __str__(self):
        return self.field.name


class AnimalAttribute(models.Model):
    animal = models.ForeignKey(Animal)
    field = models.ForeignKey(AttributeField)
    field_value = models.ForeignKey(AttributeFieldValue, null=True, blank=True)
    text_value = models.CharField(max_length=256, blank=True, null=True, db_index=True)
    int_value = models.BigIntegerField(blank=True, null=True, db_index=True)
    float_value = models.FloatField(blank=True, null=True, db_index=True)
    time_value = models.DateTimeField(blank=True, null=True, db_index=True)

    class Meta:
        verbose_name = "Animal Attribute"
        verbose_name_plural = "Animal Attributes"

    def __str__(self):
        return self.field.name


class BreedingAttribute(models.Model):
    Breeding = models.ForeignKey(Breeding)
    field = models.ForeignKey(AttributeField)
    field_value = models.ForeignKey(AttributeFieldValue, null=True, blank=True)
    text_value = models.CharField(max_length=256, blank=True, null=True, db_index=True)
    int_value = models.BigIntegerField(blank=True, null=True, db_index=True)
    float_value = models.FloatField(blank=True, null=True, db_index=True)
    time_value = models.DateTimeField(blank=True, null=True, db_index=True)

    def __str__(self):
        return self.field.name


class LogAttribute(models.Model):
    Log = models.ForeignKey(Log)
    field = models.ForeignKey(AttributeField)
    field_value = models.ForeignKey(AttributeFieldValue, null=True, blank=True)
    text_value = models.CharField(max_length=256, blank=True, null=True, db_index=True)
    int_value = models.BigIntegerField(blank=True, null=True, db_index=True)
    float_value = models.FloatField(blank=True, null=True, db_index=True)
    time_value = models.DateTimeField(blank=True, null=True, db_index=True)

    class Meta:
        verbose_name = "Log Attribute"
        verbose_name_plural = "Log Attributes"

    def __str__(self):
        return self.field.name


class Queries(models.Model):
    created_by = models.ForeignKey(User)
    date_created = models.DateTimeField()
    dataset = models.ForeignKey(AnimalDataset)
    name = models.CharField(max_length=256)
    serialised_query = models.TextField(blank=True)

    class Meta:
        verbose_name = "Query"
        verbose_name_plural = "Queries"


class Backup(models.Model):
    created_time = models.DateTimeField(blank=True)
    backupfile = models.FileField(blank=True, upload_to=backup_file_name)

    class Meta:
        verbose_name = "Backup"
        verbose_name_plural = "Backups"


class UserProfile (models.Model):
    """
    UserProfile model is used to add additional fields to Django's
    built-in auth models.
    """
    user = models.OneToOneField(User)
    default_dataset = models.ForeignKey(AnimalDataset, null=True)
    organisation = models.CharField(max_length=128, blank=True)
    phone = models.CharField(max_length=32, blank=True)

    class Meta:
        verbose_name = "User Profile"
        verbose_name_plural = "User Profiles"

    def __str__(self):
        return self.user.username
