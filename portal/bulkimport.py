from portal.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.db import transaction
from django.utils import timezone
from django.core.files.storage import default_storage
from django.core.exceptions import MultipleObjectsReturned
from django.core.context_processors import csrf
from django.db.models import Q
from datetime import datetime
import csv
import json

@login_required
def index(request):
  """
    This function handles the first step in the bulk import - the file upload. 
    It saves the file and its relevant variables and handles the rendering of the page when the user first navigates to the bulk import.
    It also handles the rendering to the second step - the settings page.
    It is called with the /portal/import/ URL.
    :param request: Django request object
    :return: HttpResponse  - Renders to the Settings step if file is uploaded correctly or returns to the file upload step if there are errors or if the link to the bulk import has just been selected
  """
  context = RequestContext(request)
  context_dict = setupContext_dict(request, "Bulk Import - File Upload")
  
  # We've got a form with POST method so update CSRF token
  context_dict.update(csrf(request))

  if request.method == 'POST':
    if 'BIfile' in request.FILES:
      request.session['file name'] = default_storage.save('temp/current_BI_file', request.FILES['BIfile'])
      context_dict['pagemsg'] = "Bulk Import Settings"
      return render_to_response('bulkimport_step2.html', context_dict, context)
    else:
      #add no file found error message!!!
      messages.add_message(request, messages.INFO, "No file found - please try uploading again")
      return render_to_response('bulkimport.html', context_dict, context)

  else:
    return render_to_response('bulkimport.html', context_dict, context)
   
def backToSettings(request):
  """
    backToSettings is a small function that handles the return to the settings page when the back button is clicked on the data definition page
    It is called using the /portal/import/bulk1/ URL.
    :param request: Django request object
    :return: HttpResponse  - Renders to the settings step of the bulk import
  """
  context = RequestContext(request)
  context_dict = setupContext_dict(request, "Bulk Import - Settings")
  return render_to_response('bulkimport_step2.html', context_dict, context)
  
@login_required
def settingsProcessing(request):
  """
    settingsProcessing processes the input from the Settings page and sets up the Data Definition step by pulling out and sorting the relevant fields from each table
    It is called using the /portal/import/bulk2/ URL.
    :param request: Django request object
    :return: HttpResponse  - Renders to the Data Definition step of the bulk import
  """
  context = RequestContext(request)
  context_dict = setupContext_dict(request, "Bulk Import - Data Definition")
  
  if request.method == 'POST':
    # We've got a form with POST method so update CSRF token
    context_dict.update(csrf(request))

    # process data from form
    #request.session['file_type'] = request.POST['file_type']
    request.session['data_imported'] = request.POST['data_imported']
    request.session['date_format'] = request.POST['date_format']
    
    #get no of colums from first row of file
    curfile = default_storage.open(request.session['file name'])
    dialect = csv.Sniffer().sniff(curfile.read())
    Inputreader = csv.reader(curfile, dialect)

    first_row = next(Inputreader)
    request.session['noColumns'] = len(first_row)
    
    #store first row and note if there is a header row
    request.session['has_header'] = request.POST['header_row']
    
    request.session['first_row'] = first_row
    
  context_dict['first_row'] = request.session['first_row']
  
  request.session['table_fields'] = []
  
  #check what type of data is being imported and pull out the relevent field names
  if request.session['data_imported'] == 'Log': #if importing Log's
    
    #have hard-coded the values from the Log table that are not auto generated - have also included Mark ID fields/Animal ID for identification
    list_of_fields = ['', '-- Animal Identification --','Mark Type', 'Mark ID Number', 'Animal ID', '', '--Mandatory Fields--', 'Log Date']
    request.session['table_fields'].extend(['Mark Type', 'Mark ID Number', 'Animal ID', 'Log Date'])
    
    #pulling out all the attributes for logs and filtering those that belong to the current dataset (or are global)
    att_field_list = AttributeField.objects.filter(table='L')
    att_field_list = att_field_list.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
    
    #grouping the attributes into mandatory and optional fields
    optional_att_field_list = att_field_list.filter(is_mandatory=False)
    mand_att_field_list = att_field_list.filter(is_mandatory=True)
      
    #creating a list of the names of the mandatory fields - used in error checking
    mand_field_names = ['Log Date']
    
    #loop though results of mandatory fields and add to display list and error checking list
    for x in mand_att_field_list:
      list_of_fields.append(x.name)
      mand_field_names.append(x.name)
  
    #save list of mandatory fields to a session variable (so it can be accessed later)
    request.session['Mandatory Fields'] = mand_field_names

    #add optional fields from the Log table to the display list
    list_of_fields.extend(['', '--Optional Fields --', 'Comments', 'Season', 'Sample Taken'])
    request.session['table_fields'].extend(['Comments', 'Season', 'Sample Taken'])
    
    #add optional fields from the Log attributes table to the display list
    for x in optional_att_field_list:
      list_of_fields.append(x.name)
   
    #add the option to ignore a column
    list_of_fields.extend(['','--Ignore Field --', ''])
 
    #add display list to context dictionary for rendering
    context_dict['list_of_fields'] = list_of_fields
    
  #IF Importing new animals
  elif request.session['data_imported'] == 'Animal':
    
    #add the mandatory variables from the Animal table to the display list
    list_of_fields = ['', '--Mandatory Animal Details--', 'Gender' ]
    request.session['table_fields'].append('Gender')
    
    #pull out all the Animal Attributes that are linked to the current dataset or are global
    att_field_list = AttributeField.objects.filter(table='A')
    att_field_list = att_field_list.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))

    #split attributes into mandatory and optional attributes
    optional_att_field_list = att_field_list.filter(is_mandatory=False)
    mand_att_field_list = att_field_list.filter(is_mandatory=True)
    
    #create a list of mandatory variables for error checking
    mand_field_names = ['Gender']
    
    #add all mandatory variables to the display and error checking lists
    for x in mand_att_field_list:
      list_of_fields.append(x.name)
      mand_field_names.append(x.name)
    
    #add optional fields from Animal Table to the display list
    list_of_fields.extend(['', '--Optional Animal Fields --', 'Comments'])
    request.session['table_fields'].append('Comments')
    
    #add optional fields from the Animal Attributes table to display list
    for x in optional_att_field_list:
     list_of_fields.append(x.name)
    
    #add mark data
    list_of_fields.extend(['', '--Mark Data --', 'Mark Type', 'Mark Number', 'Date Attached'])
    request.session['table_fields'].extend(['Mark Type', 'Mark Number', 'Date Attached'])
    mand_field_names.extend(['Mark Type', 'Mark Number', 'Date Attached'])
    att_field_list = AttributeField.objects.filter(table='M')
    att_field_list = att_field_list.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
    
    #split attributes into mandatory and optional attributes
    optional_att_field_list = att_field_list.filter(is_mandatory=False)
    mand_att_field_list = att_field_list.filter(is_mandatory=True)
    
    for x in mand_att_field_list:
      list_of_fields.append(x.name)
      mand_field_names.append(x.name)
      
    #save list of mandatory fields to a session variable (so it can be accessed later)
    request.session['Mandatory Fields'] = mand_field_names

    list_of_fields.extend(['', '--Optional Mark Fields --'])
    
    for x in optional_att_field_list:
     list_of_fields.append(x.name)
     
    list_of_fields.extend(['','--Ignore Field --', ''])
    
    #add display list to context dictionary for rendering
    context_dict['list_of_fields'] = list_of_fields
      
  elif request.session['data_imported'] == 'Mark': 
    #need to identify Animal - old mark or ID
    list_of_fields = ['', '-- Animal Identification --','Existing Mark Type', 'Existing Mark ID Number', 'Animal ID', '', '--Mandatory Fields--', 'New Mark Type', 'New Mark ID Number', 'Date Attached' ]
    request.session['table_fields'].extend(['Existing Mark Type', 'Existing Mark ID Number', 'Animal ID', 'New Mark Type', 'New Mark ID Number', 'Date Attached'])
    
    #creating a list of the names of the mandatory fields - used in error checking
    mand_field_names = ['New Mark Type', 'New Mark ID Number', 'Date Attached']
   
   #pulling out all the attributes for logs and filtering those that belong to the current dataset (or are global)
    att_field_list = AttributeField.objects.filter(table='M')
    att_field_list = att_field_list.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
    
    #grouping the attributes into mandatory and optional fields
    optional_att_field_list = att_field_list.filter(is_mandatory=False)
    mand_att_field_list = att_field_list.filter(is_mandatory=True)
    
    #loop though results of mandatory fields and add to display list and error checking list
    for x in mand_att_field_list:
      list_of_fields.append(x.name)
      mand_field_names.append(x.name)
    
    #save list of mandatory fields to a session variable (so it can be accessed later)
    request.session['Mandatory Fields'] = mand_field_names

    # Add optional mark fields to list of fields to be displayed
    list_of_fields.extend(['','--Optional Fields--', 'Comments', 'Is Present'])
    request.session['table_fields'].extend(['Comments', 'Is Present'])
    
    #add optional fields from the Mark Attribute table to the display list
    for x in optional_att_field_list:
      list_of_fields.append(x.name)
      
    #add the option to ignore a column
    list_of_fields.extend(['','--Ignore Field --', ''])
    
    #add display list to context dictionary for rendering
    context_dict['list_of_fields'] = list_of_fields
    
  elif request.session['data_imported'] == 'Breeding':
 
    #have hard-coded the values from the Log table and Breeding tables that are not auto generated - have also included Mark ID fields/Animal ID for identification
    list_of_fields = ['', '-- Animal Identification --','Mark Type', 'Mark ID Number', 'Animal ID', '', '--Mandatory Fields--', 'Log Date' ]
    
    request.session['table_fields'].extend(['Mark Type', 'Mark ID Number', 'Animal ID','Log Date' ])
    
    #pulling out all the attributes for logs and filtering those that belong to the current dataset (or are global)
    att_field_list_log = AttributeField.objects.filter(table='L')
    att_field_list_log = att_field_list_log.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
    
    #pulling out all the attributes for breeding logs and filtering those that belong to the current dataset (or are global)
    att_field_list_breed = AttributeField.objects.filter(table='B')
    att_field_list_breed = att_field_list_breed.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
    
    #grouping the attributes into mandatory and optional fields
    optional_att_field_list_log = att_field_list_log.filter(is_mandatory=False)
    mand_att_field_list_log = att_field_list_log.filter(is_mandatory=True)
    
    optional_att_field_list_breed = att_field_list_breed.filter(is_mandatory=False)
    mand_att_field_list_breed = att_field_list_breed.filter(is_mandatory=True)
      
    #creating a list of the names of the mandatory fields - used in error checking
    mand_field_names = ['Log Date']
    
    #loop though results of mandatory fields and add to display list and error checking list
    for x in mand_att_field_list_log:
      if not x.name in list_of_fields:
        list_of_fields.append(x.name)
        mand_field_names.append(x.name)
      
    for x in mand_att_field_list_breed:
      if not x.name in list_of_fields:
        list_of_fields.append(x.name)
        mand_field_names.append(x.name)
      
    list_of_fields.extend(['', '--Breeding Party Animal Idenfication--', 'Mate Mark Type', 'Mate Mark ID', 'Mate Animal ID', 'Offspring Mark Type' , 'Offspring Mark ID', 'Offspring Animal ID'])
    request.session['table_fields'].extend(['Mate Mark Type', 'Mate Mark ID', 'Mate Animal ID', 'Offspring Mark Type' , 'Offspring Mark ID', 'Offspring Animal ID'])
    
    #save list of mandatory fields to a session variable (so it can be accessed later)
    request.session['Mandatory Fields'] = mand_field_names

    #add optional fields from the Log table to the display list
    list_of_fields.extend(['', '--Optional Fields --', 'Comments', 'Season', 'Sample Taken'])
    request.session['table_fields'].extend(['Comments', 'Season', 'Sample Taken'])
    
    #add optional fields from the Log and Breeding attributes table to the display list
    for x in optional_att_field_list_log:
      list_of_fields.append(x.name)
      
    for x in optional_att_field_list_breed:
      list_of_fields.append(x.name)
   
    #add the option to ignore a column
    list_of_fields.extend(['','--Ignore Field --', ''])
    
    #add display list to context dictionary for rendering
    context_dict['list_of_fields'] = list_of_fields
    
  else:
    pass #TO DO add error
  return render_to_response('bulkimport_step3.html', context_dict, context)
    
@login_required  
def errorCheck(request):
  """
    errorCheck takes the input from the Data Definition step and checks for input field errors. 
    It then calls the processing function and handles the next page rendering (either to the error page or final success page)
    It is called using the /portal/import/bulk3/ URL.
    :param request: Django request object
    :return: HttpResponse  - Renders to the final success/error page or redirects to the Data Definition step if there are input field errors
  """
  context = RequestContext(request)
  context_dict = setupContext_dict(request, "Bulk Import - Error Checking")
  
  # We've got a form with POST method so update CSRF token
  context_dict.update(csrf(request))
 
  #need to map input columns
  inputMap = {}
  for i in range(1, request.session['noColumns'] + 1):
    if request.POST['Column_{0}'.format(i)] in inputMap:
      messages.add_message(request, messages.INFO, "ERROR - the field '{0}' has been selected twice!".format(request.POST['Column_{0}'.format(i)]))
      return redirect('/portal/import/bulk2/') 
    elif request.POST['Column_{0}'.format(i)] == "" or request.POST['Column_{0}'.format(i)][:2] == "--":
      pass  
    else:
      inputMap[request.POST['Column_{0}'.format(i)]] = i-1;
   
 
  #ERROR CHECKING
  
  #check all mandatory fields are present
  error_fields = ""
  all_required_fields = True
  count = 0
  for field in request.session['Mandatory Fields']:
    if not (field in inputMap):
      count = count + 1
      if all_required_fields == True:
        all_required_fields = False
        error_fields = error_fields + ( " '{0}'".format(field))
      else:
        all_required_fields = False
        error_fields = error_fields + ( ", '{0}'".format(field))
    
  if all_required_fields == False:
    if count > 1:
      error_fields = error_fields.rsplit(' ', 1)
      messages.add_message(request, messages.INFO, "ERROR - the fields{0} and {1} are mandatory - please include them in the upload".format(error_fields[0], error_fields[1]))
    else:
      messages.add_message(request, messages.INFO, "ERROR - the field{0} is mandatory - please include it in the upload".format(error_fields))
    
    return redirect('/portal/import/bulk2/')
   
  if request.session['data_imported'] == 'Log' or request.session['data_imported'] == 'Breeding':
    #check there is an way to ID bird
    if not(('Animal ID' in inputMap) or ('Mark Type' in inputMap and 'Mark ID Number' in inputMap)):
      messages.add_message(request, messages.INFO, 'ERROR - you have given no way indenify the animal being logged. Please include either a Mark Identification or an Animal ID')
      return redirect('/portal/import/bulk2/')   
      
  if request.session['data_imported'] == 'Mark':
    #check there is an way to ID bird
    if not(('Animal ID' in inputMap) or ('Existing Mark Type' in inputMap and 'Existing Mark ID Number' in inputMap)):
      messages.add_message(request, messages.INFO, 'ERROR - you have given no way indenify the animal who the mark has been attached to. Please include either a Mark Identification or an Animal ID')
      return redirect('/portal/import/bulk2/')   
      
  context_dict = processing(inputMap, request, context_dict)  
  return render_to_response('bulkimport_stepFinal.html', context_dict, context)
 
def processing(inputMap, request, context_dict):
  """
    processing is a function which directs control to the appropriate sub-processing function, handles the variable clean-up and success/error message handling and rendering
    It is called from the errorCheck function
    :param request: Django request object
    :param inputMap: A python dictionary that defines which fields will have data inserted into them and in which column of the csv file  the data for them can be found (generated in errorCheck)
    :param context_dict: A dictionary containing data to be used for rendering the next page
    :return: context_dict - an updated context_dict containing the outcome of the processing
  """

  #actually undertake the processing and saving of the data
  if request.session['data_imported'] == 'Animal': 
    out = processAnimal(inputMap, request)
    if out[0] == 0:
      if request.session['has_header'] == 'True':
        out[2] = out[2] - 1
      context_dict['Outcome_Message'] = "Success"
      context_dict['Count_Message'] = "You  have successfully imported {0} New Animals".format(out[2])
      
    else:
      context_dict['Outcome_Message'] = "Error"
      context_dict['Count_Message'] = "There was a error on row {0}\n {1}".format(out[2], out[1])
    
  elif request.session['data_imported'] == 'Mark':
    out = processMark(inputMap, request)
    if out[0] == 0:
      if request.session['has_header'] == 'True':
        out[2] = out[2] - 1
      context_dict['Outcome_Message'] = "Success"
      context_dict['Count_Message'] = "You  have successfully imported {0} Marks".format(out[2])
      
    else:
      context_dict['Outcome_Message'] = "Error"
      context_dict['Count_Message'] = "There was a error on row {0}\n {1}".format(out[2], out[1])
    
  elif request.session['data_imported'] == 'Log':
    out = processLog(inputMap, request)
    if out[0] == 0:
      if request.session['has_header'] == 'True':
        out[2] = out[2] - 1
      context_dict['Outcome_Message'] = "Success"
      context_dict['Count_Message'] = "You  have successfully imported {0} Logs".format(out[2])
      
    else:
      context_dict['Outcome_Message'] = "Error"
      context_dict['Count_Message'] = "There was a error on row {0}\n {1}".format(out[2], out[1])
    
  elif request.session['data_imported'] == "Breeding":
    out = processBreeding(inputMap, request)
    if out[0] == 0:
      if request.session['has_header'] == 'True':
        out[2] = out[2] - 1
      context_dict['Outcome_Message'] = "Success"
      context_dict['Count_Message'] = "You  have successfully imported {0} Breeding Logs".format(out[2])
      
    else:
      context_dict['Outcome_Message'] = "Error"
      context_dict['Count_Message'] = "There was a error on row {0}\n {1}".format(out[2], out[1])
      
  context_dict['error'] = out[0]
    
  #TO DO - Return to error screen if needed
  deleteTmpFiles(request) 
  return context_dict

def processAnimal(inputMap, request):
  """
    processAnimal is a funtion which creates and inserts into the database a set of New Animal records and their Mark records (with their associated attribute records).These records are generated from an uploaded csv file
    It is called from the processing function
    :param request: Django request object
    :param inputMap: A python dictionary that defines which fields will have data inserted into them and in which column of the csv file  the data for them can be found (generated in errorCheck)
    :return: output - a list containing 3 variables. The first defines whether the processing completed without error - defined as 0 if it finished and 1 if it did not. The second variable returns a string with an error/success message. The third variable returns the number of rows processed
  """
  context_dict = setupContext_dict(request, "Bulk Import - Processing")
  #automatic fields
  now = datetime.now()
  ds = context_dict['cur_ds']
  user=request.user
  
  output = [0, 'Success', 0]
  
  curfile = default_storage.open(request.session['file name'])
  dialect = csv.Sniffer().sniff(curfile.read())
  Inputreader = csv.reader(curfile, dialect)
  
  MAttrubutes = []
  AAttrubutes = []
  count = 0
  
  attr = AttributeField.objects.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
  attr = attr.filter(Q(table = 'M') | Q(table = 'A'))
  with transaction.atomic():
    
    for row in Inputreader:
      if count != 0 or request.session['has_header'] == 'False':
        if 'Comments' in inputMap:
          comm = row[inputMap['Comments']]  
        else:
          comm = ""
          
        if 'Is Present' in inputMap:
          is_pres = row[inputMap['Comments']] 
        else:
          is_pres = True
          
        dateAttached = datetime.strptime(row[inputMap['Date Attached']], request.session['date_format'])
        
        if timezone.is_naive(dateAttached):
          dateAttached = timezone.make_aware(dateAttached, timezone.get_current_timezone())
         
        a= Animal.objects.create(dataset=ds, gender=row[inputMap['Gender']], comments=comm, date_created=now, created_by=user, date_modified=now, modified_by=user)
        #inputtedAnimal.append[a]
        
        #add log attached when the code for it is complete
        m = Mark.objects.create(animal=a, comments=comm, mark_number=row[inputMap['Mark Number']], mark_type=row[inputMap['Mark Type']], is_present=is_pres, date_attached=dateAttached, date_created=now, created_by=user, date_modified=now, modified_by=user)
        #inputtedMark.append[m]
        
        for key in inputMap:
          if key not in request.session['table_fields']:
            try:
              current = attr.get(name=key)
            except  MultipleObjectsReturned:
              #more than one with same name - fill in all
              current = attr.filter(name=key)
              for item in current:
                if item.table =='M': #is mark attribute
                  MAttrubutes.append(createAttribute(item, row, inputMap, m, request))
                elif item.table == 'A':
                  AAttrubutes.append(createAttribute(item, row, inputMap, a, request))
            else:
              if current.table =='M': #is mark attribute
                MAttrubutes.append(createAttribute(current, row, inputMap, m, request))
              elif current.table == 'A':
                AAttrubutes.append(createAttribute(current, row, inputMap, a, request))
      
      count = count +1 
    
    AnimalAttribute.objects.bulk_create(AAttrubutes)
    MarkAttribute.objects.bulk_create(MAttrubutes)

    output[2] = count
    
    
  return output  

def processLog(inputMap, request):
  """
    processLog is a function which creates and inserts into the database a set of Log records (with their associated Log attribute records).These records are generated from an uploaded csv file 
    It is called from the processing function
    :param request: Django request object
    :param inputMap: A python dictionary that defines which fields will have data inserted into them and in which column of the csv file  the data for them can be found (generated in errorCheck)
    :return: output - a list containing 3 variables. The first defines whether the processing completed without error - defined as 0 if it finished and 1 if it did not. The second variable returns a string with an error/success message. The third variable returns the number of rows processed
  """
  context_dict = setupContext_dict(request, "Bulk Import - Processing")
  
  # generate automatic fields
  now = datetime.now()
  ds = context_dict['cur_ds']
  user=request.user
  
  #open file
  curfile = default_storage.open(request.session['file name'])
  dialect = csv.Sniffer().sniff(curfile.read())
  Inputreader = csv.reader(curfile, dialect)
 
  LAttrubutes = []
  count = 0
  output = [0, 'Success', 0]
  
  attr = AttributeField.objects.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
  attr = attr.filter(table = 'L')
  
  with transaction.atomic():
    for row in Inputreader:
      if count != 0 or request.session['has_header'] == 'False':
        
        if 'Comments' in inputMap:
          comm = row[inputMap['Comments']]  
        else:
          comm = ""
          
        if 'Season' in inputMap:
          sesn =  row[inputMap['Season']]  
        else:
          sesn = ""
          
        if 'Sample Taken' in inputMap:
          st = row[inputMap['Sample Taken']]
        else:
          st = False
          
      
        LogDate = datetime.strptime(row[inputMap['Log Date']], request.session['date_format'])
        
        if timezone.is_naive(LogDate):
          LogDate= timezone.make_aware(LogDate, timezone.get_current_timezone())
        
        if 'Animal ID' in inputMap and row[inputMap['Animal ID']] !="":
          a = Animal.objects.get(pk=row[inputMap['Animal ID']])
         
        if 'Mark Type' in inputMap:
          try:
            amark = Mark.objects.get(mark_type=row[inputMap['Mark Type']], mark_number=row[inputMap['Mark ID Number']])
            a = amark.animal
          except  MultipleObjectsReturned:
            pass #TO DO - DD ERROR HERE!!!
        
        l = Log.objects.create(animal=a, log_date=LogDate, log_type ='R', season=sesn, comments=comm, sample_taken=st, date_created = now, created_by=user, date_modified=now, modified_by= user)
          
        for key in inputMap:
          if key not in request.session['table_fields']:
            try:
              current = attr.get(name=key)
            except  MultipleObjectsReturned:
              #more than one with same name - fill in all
              current = attr.filter(name=key)
              for item in current:
                LAttrubutes.append(createAttribute(item, row, inputMap, l, request))
            else:
              LAttrubutes.append(createAttribute(current, row, inputMap, l, request))
              
      
      count = count +1 
    
    LogAttribute.objects.bulk_create(LAttrubutes)
    
    output[2] = count

  return output
   
def processBreeding(inputMap, request):
  """
   processBreeding is a function which creates and inserts into the database a set of Breeding and Log records (with their associated attribute records).These records are generated from an uploaded csv file 
   It is called from the processing function
   :param request: Django request object
   :param inputMap: A python dictionary that defines which fields will have data inserted into them and in which column of the csv file  the data for them can be found (generated in errorCheck)
   :return: output - a list containing 3 variables. The first defines whether the processing completed without error - defined as 0 if it finished and 1 if it did not. The second variable returns a string with an error/success message. The third variable returns the number of rows processed
   """
  context_dict = setupContext_dict(request, "Bulk Import - Processing")
  #automatic fields
  now = datetime.now()
  ds = context_dict['cur_ds']
  user=request.user
  
  curfile = default_storage.open(request.session['file name'])
  dialect = csv.Sniffer().sniff(curfile.read())
  Inputreader = csv.reader(curfile, dialect)
  
  LAttrubutes = []
  BAttrubutes = []
  count = 1
  output = [0, 'Success', 0]
  
  attr = AttributeField.objects.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
  attr = attr.filter(Q(table = 'B') | Q(table = 'L'))
  with transaction.atomic():
    for row in Inputreader:
      if count != 1 or request.session['has_header'] == 'False':
        if 'Comments' in inputMap:
          comm = row[inputMap['Comments']]  
        else:
          comm = ""
          
        if 'Season' in inputMap:
          sesn =  row[inputMap['Season']]  
        else:
          sesn = ""
          
        if 'Sample Taken' in inputMap:
          st = row[inputMap['Sample Taken']]
        else:
          st = False

        LogDate = datetime.strptime(row[inputMap['Log Date']], request.session['date_format'])
        
        if timezone.is_naive(LogDate):
          dateAttached = timezone.make_aware(LogDate, timezone.get_current_timezone())
          
        if 'Animal ID' in inputMap and row[inputMap['Animal ID']] !="":
          a = Animal.objects.get(pk=row[inputMap['Animal ID']])
         
        if 'Mark Type' in inputMap:
          try:
            with transaction.atomic():
              amark = Mark.objects.get(mark_type=row[inputMap['Mark Type']], mark_number=row[inputMap['Mark ID Number']])
              a = amark.animal
          except  MultipleObjectsReturned:
            out[0] = 1
            out[1] = 'The Mark you have used to identify this animal is not unique - therefore the animal cannot be found./n Please use an Animal ID or another Mark to identify this animal, or edit it manually'
            out[2] = count
            return out
          except DoesNotExist:
            out[0] = 1
            out[1] = 'The Mark you have used to identify this Animal is not in the database./n Please check the Mark Type and Mark ID Number to check they are correct'
            out[2] = count
            return out
        
        if 'Mate Animal ID' in inputMap and row[inputMap['Mate Animal ID']] !="":
          amate = Animal.objects.get(pk=row[inputMap['Mate Animal ID']])
        else:
          if 'Mate Mark Type' in inputMap:
            try:
              with transaction.atomic():
                matemark = Mark.objects.get(mark_type=row[inputMap['Mate Mark Type']], mark_number=row[inputMap['Mate Mark ID']])
                amate = matemark.animal
            except  MultipleObjectsReturned:
              out[0] = 1
              out[1] = "The Mark you have used to identify this Animal's mate is not unique - therefore it cannot be found./n Please use an Animal ID or another Mark to identify the mate"
              out[2] = count
              return out
            except DoesNotExist:
              out[0] = 1
              out[1] = "The Mark you have used to identify this Animal's mate is not in the database./n Please check the Mark Type and Mark ID Number to check they are correct"
              out[2] = count
              return out
          
        if 'Offspring Animal ID' in inputMap and row[inputMap['Offspring Animal ID']] !="":
          aoffspring = Animal.objects.get(pk=row[inputMap['Offspring Animal ID']])
        else:
          if 'Offspring Mark Type' in inputMap:
            try:
              with transaction.atomic():
                offmark = Mark.objects.get(mark_type=row[inputMap['Offspring Mark Type']], mark_number=row[inputMap['Offspring Mark ID']])
                aoffspring = matemark.animal
            except  MultipleObjectsReturned:
              out[0] = 1
              out[1] = "The Mark you have used to identify this Animal's Offspring is not unique - therefore it cannot be found./n Please use an Animal ID or another Mark to identify the offspring"
              out[2] = count
              return out
            except ObjectDoesNotExist:
              out[0] = 1
              out[1] = "The Mark you have used to identify this Animal's Offspring is not in the database./n Please check the Mark Type and Mark ID Number to check they are correct"
              out[2] = count
              return out
          
       
        l = Log.objects.create(animal=a, log_date=LogDate, log_type="B", season=sesn, comments=comm, sample_taken=st, date_created=now, created_by=user, date_modified=now, modified_by=user)  
        
        b = Breeding.objects.create(log=l, mate=amate, offspring=aoffspring, season=sesn, comments=comm, date_created=now, created_by=user, date_modified=now, modified_by=user)
        
        for key in inputMap:
          if key not in request.session['table_fields']:
            try:
              current = attr.get(name=key)
            except  MultipleObjectsReturned:
              #more than one with same name - fill in all
              current = attr.filter(name=key)
              for item in current:
                if item.table =='L': #is log attribute
                  LAttrubutes.append(createAttribute(item, row, inputMap, l, request))
                elif item.table == 'B':
                  BAttrubutes.append(createAttribute(item, row, inputMap, b, request))
            else:
              if current.table =='L': #is log attribute
                LAttrubutes.append(createAttribute(current, row, inputMap, l, request))
              elif current.table == 'B':
                BAttrubutes.append(createAttribute(current, row, inputMap, b, request))
      
      count = count +1 
    if LAttrubutes:
      LogAttribute.objects.bulk_create(LAttrubutes)
    if BAttrubutes:
      BreedingAttribute.objects.bulk_create(BAttrubutes)
      
    output[2] = count
  

  return output
   
def processMark(inputMap, request):
  """
    processMark is a function which creates and inserts into the database a set of Mark records (with their associated Mark attributes).These records are generated from an uploaded csv file    
    It is called from the processing function
    :param request: Django request object
    :param inputMap: A python dictionary that defines which fields will have data inserted into them and in which column of the csv file  the data for them can be found (generated in errorCheck)
    :return: output - a list containing 3 variables. The first defines whether the processing completed without error - defined as 0 if it finished and 1 if it did not. The second variable returns a string with an error/success message. The third variable returns the number of rows processed
  """
  
  context_dict = setupContext_dict(request, "Bulk Import - Processing")
  # generate automatic fields
  now = datetime.now()
  ds = context_dict['cur_ds']
  user=request.user
  
  #open file
  curfile = default_storage.open(request.session['file name'])
  dialect = csv.Sniffer().sniff(curfile.read())
  Inputreader = csv.reader(curfile, dialect)
  

  MAttrubutes = []
  count = 1
  output = [0, 'Success', 0]
  
  attr = AttributeField.objects.filter(Q(is_global = True) | Q(dataset = request.session['cur_ds']))
  attr = attr.filter(table = 'M')
  
  with transaction.atomic():
    for row in Inputreader:
      if count != 1 or request.session['has_header'] == 'False':
        if 'Comments' in inputMap:
          comm = row[inputMap['Comments']]  
        else:
          comm = ""
          
        if 'Is Present' in inputMap:
          is_pres = row[inputMap['Comments']] 
        else:
          is_pres = True
          
        dateAttached = datetime.strptime(row[inputMap['Date Attached']], request.session['date_format'])
        
        if timezone.is_naive(dateAttached):
          dateAttached = timezone.make_aware(dateAttached, timezone.get_current_timezone())
        
        if 'Animal ID' in inputMap and row[inputMap['Animal ID']] != "":
          a = Animal.objects.get(pk=row[inputMap['Animal ID']])
         
        if 'Existing Mark Type' in inputMap:
          try:
            with transaction.atomic():
              oldMark = Mark.objects.get(mark_type=row[inputMap['Existing Mark Type']], mark_number=row[inputMap['Existing Mark ID Number']])
              a = oldMark.animal
          except MultipleObjectsReturned:
            out[0] = 1
            out[1] = 'The Mark you have used to identify this animal is not unique - therefore the animal cannot be found./n Please use an Animal ID or another Mark to identify this animal, or edit it manually'
            out[2] = count
            return out
          except ObjectDoesNotExist:
            out[0] = 1
            out[1] = 'The Mark you have used to identify this Animal is not in the database./n Please check the Mark Type and Mark ID Number to check they are correct'
            out[2] = count
            return out
        
        m = Mark.objects.create(animal=a, comments=comm, mark_number=row[inputMap['New Mark ID Number']], mark_type=row[inputMap['New Mark Type']], is_present=is_pres, date_attached=dateAttached, date_created=now, created_by=user, date_modified=now, modified_by=user)
          
        for key in inputMap:
         
          if key not in request.session['table_fields']:
            try:
              current = attr.get(name=key, table='M')
            except  MultipleObjectsReturned:
              #more than one with same name - fill in all
              current = attr.filter(name=key, table='M')
              for item in current:
                  MAttrubutes.append(createAttribute(item, row, inputMap, m, request))
            else:
              MAttrubutes.append(createAttribute(current, row, inputMap, m, request))
              
      
      count = count +1 
      
    
    MarkAttribute.objects.bulk_create(MAttrubutes)
    
    output[2] = count
    
  return output
    
def createAttribute(attr, row, inputMap, baseRecord, request):
  """
    createAttribute is a helper function which handles the parsing and creation of all attribute fields.    
    It is called from any of the table processing functions (processMark, processBreeding, processLog, processAnimal)
    :param request: Django request object
    :param attr: an AttributeField object - this is the attribute which will be created
    :param row: The current row being processed in the csv file
    :param baseRecord: Either an Animal, Log, Mark or Breeding object - this is the record that the attribute will be linked to
    :param inputMap: A python dictionary that defines which fields will have data inserted into them and in which column of the csv file  the data for them can be found (generated in errorCheck)
    :return: out - a AnimalAttribute/MarkAttribute/LogAttribute/BreedingAttribute object - the object the function was called to create
  """
  
  if attr.type == 'L':
    lookupval = AttributeFieldValue.objects.filter(Q(field=attr) & (Q(shortval = row[inputMap[attr.name]]) | Q(longval = row[inputMap[attr.name]])))
    if attr.table == 'A':
      out= AnimalAttribute(animal=baseRecord, field=attr, field_value=lookupval[0])
    elif attr.table == 'L':
      out = LogAttribute(Log=baseRecord, field=attr, field_value=lookupval[0])
    elif attr.table == 'M':
      out = MarkAttribute(mark=baseRecord,field=attr, field_value=lookupval[0])
    elif attr.table == 'B':
      out = BreedingAttribute(Breeding=baseRecord, field=attr, field_value=lookupval[0])
   
  elif attr.type == 'T':
    if attr.table == 'A':
      out= AnimalAttribute(animal=baseRecord, field=attr, text_value= row[inputMap[attr.name]])
    elif attr.table == 'L':
      out = LogAttribute(Log=baseRecord, field=attr, text_value=row[inputMap[attr.name]])
    elif attr.table == 'M':
      out = MarkAttribute(mark=baseRecord,field=attr, text_value=row[inputMap[attr.name]])
    elif attr.table == 'B':
      out = BreedingAttribute(Breeding=baseRecord, field=attr, text_value= row[inputMap[attr.name]])
  
  elif attr.type == 'D':
    dateData = datetime.strptime(row[inputMap[attr.name]],request.session['date_format'])
    if dateData.is_nieve():
      dateData.make_aware() 
      
    if attr.table == 'A':
      out= AnimalAttribute(animal=baseRecord, field=attr, date_value= dateData)
    elif attr.table == 'L':
      out = LogAttribute(Log=baseRecord, field=attr, date_value= dateData)
    elif attr.table == 'M':
      out = MarkAttribute(mark=baseRecord,field=attr, date_value= dateData)
    elif attr.table == 'B':
      out = BreedingAttribute(Breeding=baseRecord, field=attr, date_value= dateData)
      
  elif attr.type == 'I':
    if attr.table == 'A':
      out= AnimalAttribute(animal=baseRecord, field=attr, int_value= row[inputMap[attr.name]])
    elif attr.table == 'L':
      out = LogAttribute(Log=baseRecord, field=attr, int_value= row[inputMap[attr.name]])
    elif attr.table == 'M':
      out = MarkAttribute(mark=baseRecord,field=attr, int_value= row[inputMap[attr.name]])
    elif attr.table == 'B':
      out = BreedingAttribute(Breeding=baseRecord, field=attr, int_value= row[inputMap[attr.name]])
  
  elif attr.type == 'F':
    if attr.table == 'A':
      out= AnimalAttribute(animal=baseRecord, field=attr, float_value= row[inputMap[attr.name]])
    elif attr.table == 'L':
      out = LogAttribute(Log=baseRecord, field=attr, float_value= row[inputMap[attr.name]])
    elif attr.table == 'M':
      out = MarkAttribute(mark=baseRecord,field=attr, float_value= row[inputMap[attr.name]])
    elif attr.table == 'B':
      out = BreedingAttribute(Breeding=baseRecord, field=attr, float_value= row[inputMap[attr.name]])
  
  return out
  
def setupContext_dict(request, pageMessage):
  """
    setupContext_dict is a helper function which handles creation of the context dictionary for each new view  
    :param request: Django request object
    :param pageMessage: A string object containing the page message for the next view
    :return: context_dict - a python dictionary containing variables needed for the rendering of the next view
  """

  context_dict = { 'pagetitle':	'Threatened Species Portal',
        'pagemsg': pageMessage,
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  return context_dict

def deleteTmpFiles(request):
  """
    setupContext_dict is a helper function which handles the deletion of the session variables and temporary files generated by the bulk import
    :param request: Django request object
    :return: returns nothing
  """

  #delete temporary file
  default_storage.delete(request.session['file name'])
  
  #delete all created session variables
  del request.session['file name']
  #del request.session['file_type']
  del request.session['data_imported']
  del request.session['date_format'] 
  del request.session['noColumns']
  del request.session['has_header']
  del request.session['first_row'] 
  del request.session['table_fields']
  del request.session['Mandatory Fields']
  
  return
