from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from portal.models import AnimalDataset

@login_required
def index(request):
  context = RequestContext(request)
  context_dict = { 'pagetitle':	'Threatened Species Portal',
        'pagemsg': 'Keeping Track of Wildlife',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # Prepare the context
  context_dict['def_ds'] = 'Shy Albatross'
  context_dict['datasets'] = dataset_list

  return render_to_response('group.html', context_dict, context)
