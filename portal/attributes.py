from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from portal.models import *

from datetime import datetime




@login_required
def index_of_mark_attr(request):
    """

    """
    context_dict = { 'pagetitle':    'Threatened Species Portal',
        'pagemsg': 'Mark Attribute',
        'title': 'Mark',
        'url':'/portal/markattributes/'
        }
    uo = request.user
    context_dict['user'] = uo
    up = UserProfile.objects.get(user=uo)
    context_dict.update(csrf(request))

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    dataset_list = AnimalDataset.objects.order_by('name')
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    # Prepare the context
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    if request.method == 'POST':
        for (k, v) in request.POST.items():
            if k.find('name') != -1:
                atts = AttributeField.objects.filter(dataset=cur_ds, table='M', name=v.strip(''))
                is_mandatory = False
                if request.POST.get('mandatory', default='false') != 'false':
                    is_mandatory = True
                is_global = False
                if request.POST.get('global', default='false') != 'false':
                    is_global = True
                if len(atts) == 0:
                    att = AttributeField(dataset=cur_ds, table='M', name=v.strip(''),
                                         type=request.POST.get('type', default='Text')[0],
                                         description=request.POST.get('desc', default=''),
                                         is_mandatory=is_mandatory,
                                         is_global=is_global)
                    att.save()
                else :
                    att = atts[0]
                    att.name = v.strip('')
                    att.is_global = is_global
                    att.is_mandatory = is_mandatory
                    att.description = request.POST.get('desc', default='')
                    att.type = request.POST.get('type', default='Text')[0]
                    att.save()


                AttributeFieldValue.objects.filter(field=att).delete()
                for (i, j) in request.POST.items():
                    if i.find('values') != -1:
                        vlist = j.split(',')
                        for i in range(len(vlist) / 2):
                            a = AttributeFieldValue(field=att, shortval=vlist[i * 2], longval=vlist[i * 2 + 1])
                            a.save()

        return HttpResponseRedirect('/portal/markattributes/')
    else :
        id = request.GET.get('id', default=0)
        if id != 0 :
            att = AttributeField.objects.get(id=id)
            AttributeFieldValue.objects.filter(field=att).delete()
            att.delete()
        attr_list = []
        attrs = AttributeField.objects.filter(dataset=cur_ds, table='M')
        for att in attrs :
            vlist = []
            values = AttributeFieldValue.objects.filter(field=att)
            for v in values :
                vlist.append({'short':v.shortval, 'long':v.longval})
            attr_list.append({'vlist':vlist, 'att':att})
        context_dict['attrs'] = attr_list
        return render_to_response('attributes.html', context_dict)





@login_required
def index_of_animal_attr(request):
    """

    """
    context_dict = { 'pagetitle':    'Threatened Species Portal',
        'pagemsg': 'Animal Attribute',
        'title': 'Animal',
        'url':'/portal/animalattributes/',
        }
    uo = request.user
    context_dict['user'] = uo
    up = UserProfile.objects.get(user=uo)
    context_dict.update(csrf(request))

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    dataset_list = AnimalDataset.objects.order_by('name')
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    # Prepare the context
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    if request.method == 'POST':
        for (k, v) in request.POST.items():
            if k.find('name') != -1:
                atts = AttributeField.objects.filter(dataset=cur_ds, table='A', name=v.strip(''))
                is_mandatory = False
                if request.POST.get('mandatory', default='false') != 'false':
                    is_mandatory = True
                is_global = False
                if request.POST.get('global', default='false') != 'false':
                    is_global = True
                if len(atts) == 0:
                    att = AttributeField(dataset=cur_ds, table='A', name=v.strip(''),
                                         type=request.POST.get('type', default='Text')[0],
                                         description=request.POST.get('desc', default=''),
                                         is_mandatory=is_mandatory,
                                         is_global=is_global)
                    att.save()
                else :
                    att = atts[0]
                    att.name = v.strip('')
                    att.is_global = is_global
                    att.is_mandatory = is_mandatory
                    att.description = request.POST.get('desc', default='')
                    att.type = request.POST.get('type', default='Text')[0]
                    att.save()


                AttributeFieldValue.objects.filter(field=att).delete()
                for (i, j) in request.POST.items():
                    if i.find('values') != -1:
                        vlist = j.split(',')
                        for i in range(len(vlist) / 2):
                            a = AttributeFieldValue(field=att, shortval=vlist[i * 2], longval=vlist[i * 2 + 1])
                            a.save()

        return HttpResponseRedirect('/portal/animalattributes/')
    else :
        id = request.GET.get('id', default=0)
        if id != 0 :
            att = AttributeField.objects.get(id=id)
            AttributeFieldValue.objects.filter(field=att).delete()
            att.delete()
        attr_list = []
        attrs = AttributeField.objects.filter(dataset=cur_ds, table='A')
        for att in attrs :
            vlist = []
            values = AttributeFieldValue.objects.filter(field=att)
            for v in values :
                vlist.append({'short':v.shortval, 'long':v.longval})
            attr_list.append({'vlist':vlist, 'att':att})
        context_dict['attrs'] = attr_list
        return render_to_response('attributes.html', context_dict)





@login_required
def index_of_log_attr(request):
    """

    """
    """

    """
    context_dict = { 'pagetitle':    'Threatened Species Portal',
        'pagemsg': 'Log Attribute',
        'title': 'Log',
        'url':'/portal/logattributes/'
        }
    uo = request.user
    context_dict['user'] = uo
    up = UserProfile.objects.get(user=uo)
    context_dict.update(csrf(request))

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    dataset_list = AnimalDataset.objects.order_by('name')
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    # Prepare the context
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    if request.method == 'POST':
        for (k, v) in request.POST.items():
            if k.find('name') != -1:
                atts = AttributeField.objects.filter(dataset=cur_ds, table='L', name=v.strip(''))
                is_mandatory = False
                if request.POST.get('mandatory', default='false') != 'false':
                    is_mandatory = True
                is_global = False
                if request.POST.get('global', default='false') != 'false':
                    is_global = True
                if len(atts) == 0:
                    att = AttributeField(dataset=cur_ds, table='L', name=v.strip(''),
                                         type=request.POST.get('type', default='Text')[0],
                                         description=request.POST.get('desc', default=''),
                                         is_mandatory=is_mandatory,
                                         is_global=is_global)
                    att.save()
                else :
                    att = atts[0]
                    att.name = v.strip('')
                    att.is_global = is_global
                    att.is_mandatory = is_mandatory
                    att.description = request.POST.get('desc', default='')
                    att.type = request.POST.get('type', default='Text')[0]
                    att.save()


                AttributeFieldValue.objects.filter(field=att).delete()
                for (i, j) in request.POST.items():
                    if i.find('values') != -1:
                        vlist = j.split(',')
                        for i in range(len(vlist) / 2):
                            a = AttributeFieldValue(field=att, shortval=vlist[i * 2], longval=vlist[i * 2 + 1])
                            a.save()

        return HttpResponseRedirect('/portal/logattributes/')
    else :
        id = request.GET.get('id', default=0)
        if id != 0 :
            att = AttributeField.objects.get(id=id)
            AttributeFieldValue.objects.filter(field=att).delete()
            att.delete()
        attr_list = []
        attrs = AttributeField.objects.filter(dataset=cur_ds, table='L')
        for att in attrs :
            vlist = []
            values = AttributeFieldValue.objects.filter(field=att)
            for v in values :
                vlist.append({'short':v.shortval, 'long':v.longval})
            attr_list.append({'vlist':vlist, 'att':att})
        context_dict['attrs'] = attr_list
        return render_to_response('attributes.html', context_dict)


@login_required
def index_of_breeding_attr(request):
    """

    """
    context_dict = { 'pagetitle':    'Threatened Species Portal',
        'pagemsg': 'Breeding Attribute',
        'title': 'Breeding',
        'url':'/portal/breedingattributes/'
        }
    uo = request.user
    context_dict['user'] = uo
    up = UserProfile.objects.get(user=uo)
    context_dict.update(csrf(request))

    # Grabs current dataset (if not available set it)
    if (request.session.get('cur_ds') is None):
        if (up.default_dataset is None):
            request.session['cur_ds'] = 1
        else:
            request.session['cur_ds'] = up.default_dataset.pk

    dataset_list = AnimalDataset.objects.order_by('name')
    cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
    # Prepare the context
    context_dict['cur_ds'] = cur_ds
    context_dict['datasets'] = dataset_list

    if request.method == 'POST':
        for (k, v) in request.POST.items():
            if k.find('name') != -1:
                atts = AttributeField.objects.filter(dataset=cur_ds, table='B', name=v.strip(''))
                is_mandatory = False
                if request.POST.get('mandatory', default='false') != 'false':
                    is_mandatory = True
                is_global = False
                if request.POST.get('global', default='false') != 'false':
                    is_global = True
                if len(atts) == 0:
                    att = AttributeField(dataset=cur_ds, table='B', name=v.strip(''),
                                         type=request.POST.get('type', default='Text')[0],
                                         description=request.POST.get('desc', default=''),
                                         is_mandatory=is_mandatory,
                                         is_global=is_global)
                    att.save()
                else :
                    att = atts[0]
                    att.name = v.strip('')
                    att.is_global = is_global
                    att.is_mandatory = is_mandatory
                    att.description = request.POST.get('desc', default='')
                    att.type = request.POST.get('type', default='Text')[0]
                    att.save()


                AttributeFieldValue.objects.filter(field=att).delete()
                for (i, j) in request.POST.items():
                    if i.find('values') != -1:
                        vlist = j.split(',')
                        for i in range(len(vlist) / 2):
                            a = AttributeFieldValue(field=att, shortval=vlist[i * 2], longval=vlist[i * 2 + 1])
                            a.save()

        return HttpResponseRedirect('/portal/breedingattributes/')
    else :
        id = request.GET.get('id', default=0)
        if id != 0 :
            att = AttributeField.objects.get(id=id)
            AttributeFieldValue.objects.filter(field=att).delete()
            att.delete()
        attr_list = []
        attrs = AttributeField.objects.filter(dataset=cur_ds, table='B')
        for att in attrs :
            vlist = []
            values = AttributeFieldValue.objects.filter(field=att)
            for v in values :
                vlist.append({'short':v.shortval, 'long':v.longval})
            attr_list.append({'vlist':vlist, 'att':att})
        context_dict['attrs'] = attr_list
        return render_to_response('attributes.html', context_dict)

