from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from portal.models import AnimalDataset, UserProfile

@login_required
def dashboard(request):
  context = RequestContext(request)
  context_dict = { 'pagetitle':	'Threatened Species Portal',
        'pagemsg': 'Keeping Track of Wildlife',
        }
        
  # shows user details      
  uo = request.user      
  context_dict['user']= uo  
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk
          
  dataset_list = AnimalDataset.objects.order_by('name')
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))

  # Prepare the context
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  context_dict['user_profile'] = up
  context_dict['user_detail'] = uo  

  return render_to_response('dashboard.html', context_dict, context)
