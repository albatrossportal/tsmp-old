from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from portal.models import *
from datetime import datetime
import json
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect 
from django.core.exceptions import *

@login_required
def index(request):
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Keeping Track of Wildlife',
        }
  dataset_list = AnimalDataset.objects.order_by('name')
  addMarkForm = MarkForm()

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  context_dict['add_mark_form'] = addMarkForm
  #indicate adding a new log

  return render_to_response('mark.html', context_dict, context)

@login_required
def json_get_mark_types(request):
  """
  This returns a JSON object used by the search animal form
  """
  out = Mark.get_mark_types(request.session.get('cur_ds'))
  return HttpResponse(json.dumps(out), content_type="application/json")

@login_required
def json_get_mark_numbers(request):
  """
  This returns a JSON object used by the search animal form
  """
  out = Mark.get_mark_numbers(request.session.get('cur_ds'))
  return HttpResponse(json.dumps(out), content_type="application/json")

@login_required

def json_get_attribute_fieldlist(request, tbl='M'):
  """
  This returns a JSON object of the attribute fields for Mark
  """
  out = AttributeField.get_fieldlist(tbl, request.session.get('cur_ds'))
  return HttpResponse(json.dumps(out), content_type="application/json")
  
@login_required
def add(request, aid=None):
 # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add New Mark',
        }
  dataset_list = AnimalDataset.objects.order_by('name')
  if aid is not None:
    myanimal = Animal.objects.get(pk=aid)
    context_dict['cur_animal'] = myanimal
  else:
    HttpResponse("You didn't specify a mark id")

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  context_dict['addmark'] = True
  mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
  if not 'error' in mtypes:
    context_dict['mark_types'] = mtypes['mark_types']

  # We've got a form with POST method so update CSRF token
  context_dict.update(csrf(request))
  # Check if the request is someone submitting a form (POST method)
  # If not POST then it's a GET so just display the (blank) form

  # Do the same for mark attributes
  # We need to lookup the mandatory fields for the dataset and globally
  min_mark_att_fields = []
  att_field_list = AttributeField.objects.filter(table='M', is_mandatory=True, is_global=True)
  for x in att_field_list:
    min_mark_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='M', is_mandatory=True, dataset=cur_ds)
  for x in att_field_list:
    min_mark_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  context_dict['min_mark_att_fields'] = min_mark_att_fields

  # Look up all fields that can be added
  all_mark_att_fields = []
  mark_att_field_values = {}
  att_field_list = AttributeField.objects.filter(table='M', is_global=True)
  for x in att_field_list:
    all_mark_att_fields.append(x)
    if x.type == 'L':
      # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      x.vals = fvals
  att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds)
  for x in att_field_list:
    all_mark_att_fields.append(x)

  context_dict['mark_att_field_values'] = mark_att_field_values
  context_dict['all_mark_att_fields'] = all_mark_att_fields
 
  return render_to_response('mark.html', context_dict, context)


@login_required
def edit(request, mid=None):
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add Mark',
        }
		
  dataset_list = AnimalDataset.objects.order_by('name')
  
	#indicate editing a Mark
  context_dict['addmark']= False  

	# get Mark Data
  mark = Mark.objects.get(pk=mid)
  context_dict['mark']= mark


	# shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)
 
  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk
 
  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
  if not 'error' in mtypes:
    context_dict['mark_types'] = mtypes['mark_types']
   
  # We've got a form with POST method so update CSRF token
  context_dict.update(csrf(request))
  # Check if the request is someone submitting a form (POST method)
  # If not POST then it's a GET so just display the (blank) form
   
  # We need to lookup the mandatory fields for the dataset and globally
  nolist_value = ''
  min_mark_att_fields = []
  
  att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds, is_mandatory=True)
  for x in att_field_list:
    if x.type == 'L':
	  # Lookup field values and put them in an array
      fvals = AttributeFieldValue.objects.filter(field=x)
      found = False
      for y in fvals:
        #TO DO - work out if this should be a get
        selected = MarkAttribute.objects.filter(mark=mark,field=x, field_value=y)
        if len(selected) > 0:
          y.selected = 1
          found=True
        else:
          y.selected = 0
          
      if found == False:
        messages.add_message(request, messages.INFO, "There is an error in this record - the mandatory field '{0}' has no data. Please add this data while editing".format(x.name))
    elif x.type == 'I':
      try:
        nolist_value = MarkAttribute.objects.get(mark=mark, field=x).int_value
      except ObjectDoesNotExist:
        messages.add_message(request, messages.INFO, "There is an error in this record - the mandatory field '{0}' has no data. Please add this data while editing".format(x.name))
    elif x.type == 'T':
      try:
        nolist_value = MarkAttribute.objects.get(mark=mark, field=x).text_value
      except ObjectDoesNotExist:
        messages.add_message(request, messages.INFO, "There is an error in this record - the mandatory field '{0}' has no data. Please add this data while editing".format(x.name))
    elif x.type == 'F':
      try:
        nolist_value = MarkAttribute.objects.get(mark=mark, field=x).float_value
      except ObjectDoesNotExist:
        messages.add_message(request, messages.INFO, "There is an error in this record - the mandatory field '{0}' has no data. Please add this data while editing".format(x.name))
    elif x.type == 'D':
      try:
        nolist_value = MarkAttribute.objects.get(mark=mark, field=x).time_value
      except ObjectDoesNotExist:
        messages.add_message(request, messages.INFO, "There is an error in this record - the mandatory field '{0}' has no data. Please add this data while editing".format(x.name))
    x.vals = fvals
    x.nolist_value = nolist_value
    min_mark_att_fields.append(x)
    context_dict['found'] = found
  
  att_list = MarkAttribute.objects.filter(mark=mark)
  for att in att_list :
      att_f = att.field 
      if (att_f.is_mandatory == True):
          continue
      else:         
          if (att_f.type == 'L') :
	      fvals = AttributeFieldValue.objects.filter(field=att_f)
	      for y in fvals:
		  selected = MarkAttribute.objects.filter(mark=mark,field=att_f, field_value = y)
		  if len(selected) > 0:
		      y.selected = 1
		  else:
		      y.selected = 0
	  elif att_f.type == 'I':
	      nolist_value = MarkAttribute.objects.filter(mark=mark, field=att_f)[0].int_value
	  elif att_f.type == 'T':
	      nolist_value = MarkAttribute.objects.filter(mark=mark, field=att_f)[0].text_value
	  elif att_f.type == 'F':
	      nolist_value = MarkAttribute.objects.filter(mark=mark, field=att_f)[0].float_value
	  elif att_f.type == 'D':
	      nolist_value = MarkAttribute.objects.filter(mark=mark, field=att_f)[0].time_value
	  
	  att_f.vals = fvals
	  att_f.nolist_value = nolist_value
      min_mark_att_fields.append(att_f)
  context_dict['min_mark_att_fields'] = min_mark_att_fields

  # Look up all fields that can be added
  all_mark_att_fields = []
  mark_att_field_values = {}
  att_field_list = AttributeField.objects.filter(table='M', dataset=cur_ds)
  for x in att_field_list:
      if x.type == 'L':
	  # Lookup field values and put them in an array
	  fvals = AttributeFieldValue.objects.filter(field=x)
	  x.vals = fvals
      all_mark_att_fields.append(x)
  att_field_list = AttributeField.objects.filter(table='M', is_global=True).exclude(dataset=cur_ds)
  for x in att_field_list:
      if x.type == 'L':
	  # Lookup field values and put them in an array
	  fvals = AttributeFieldValue.objects.filter(field=x)
	  x.vals = fvals
      all_mark_att_fields.append(x)
  context_dict['mark_att_field_values'] = mark_att_field_values
  context_dict['all_mark_att_fields'] = all_mark_att_fields		

 
  return render_to_response('mark.html', context_dict, context)

"""
json_saveMark() adds a new Mark
"""
@login_required
@csrf_exempt
def json_saveMark(request):

  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
  'pagemsg': 'Add New Animal',
  }
  dataset_list = AnimalDataset.objects.order_by('name')

  # get user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  #Set time for creation/modification of record
  now = datetime.now()


  m = {}
  errors = {}
  if request.is_ajax() and request.method == 'POST':
    data = json.loads(request.body)
    m = data['mark']
    errors['errors'] = []
    
    
    #validation
    
    if m['mark_number'] == "":
      errors['errors'].append("Mark Number: A Mark ID must be spesified")
    
    #date
    try:
      datetime.strptime(m['mark_date_attached'],'%Y-%m-%d %H:%M:%S')
    except ValueError:
      errors['errors'].append("Date Attached: invalid format. Must be YYYY-MM-DD HH-MM-SS")
    
    date = datetime.strptime(m['mark_date_attached'],'%Y-%m-%d %H:%M:%S')

    if date > now:
      errors['errors'].append("Date Attached: Date Can not be in the future")
      
    if m['mark_is_present'] =="0" or m['mark_is_present'] =="1":

      if (m['mark_is_present'] == "0"):
	is_present=False
      else:
	is_present=True

    else:
      errors['errors'].append("Is Present: Value Invalid")
      
    #TODO validate attributes
    
    #get animal object
    try:
      animal = Animal.objects.get(pk=m['id'])
    except Animal.DoesNotExist:
      errors['errors'].append("Can not find Animal")
      
    if errors['errors']:
      return HttpResponse(json.dumps(errors), content_type="application/json")  
    
    #create mark
    mark = Mark.objects.create(animal=animal, mark_number=m['mark_number'], mark_type=m['mark_type'], is_present=is_present, comments=m['comments'], date_created=now, date_modified=now, created_by=uo, modified_by=uo, date_attached=m['mark_date_attached'])
    
    
    #save/ add attributes
    for att in m['attributes']:
	# grab the attribute
	af = AttributeField.objects.filter(is_global=True, table='M', name=att)
	if len(af) == 1:
		afield = af[0]
	elif len(af) == 0:
		af = AttributeField.objects.filter(is_global=False, table='M', dataset=cur_ds, name=att)
		if len(af) != 1:
			raise Exception("WTF dataset Log attribute field not found")
		else:
			afield = af[0]
	else:
		raise Exception("WTF 2 global Log attribute fields with the same name")

	# if lookup, grab the value
	if afield.type == 'L':
		afv = AttributeFieldValue.objects.get(shortval=m['attributes'][att], field=afield)
		# returning any more than 1 value will raise an exception which is good
		aa = MarkAttribute.objects.create(mark=mark, field=afield, field_value=afv)
	# not a lookup, just store the value in the right field
	elif afield.type == 'F':
		aa = MarkAttribute.objects.create(mark=mark, field=afield, float_value=m['attributes'][att])
	elif afield.type == 'T':
		aa = MarkAttribute.objects.create(mark=mark, field=afield, text_value=m['attributes'][att])
	elif afield.type == 'I':
		aa = MarkAttribute.objects.create(mark=mark, field=afield, int_value=m['attributes'][att])
	elif afield.type == 'D':
		d = datetime.strptime(m['attributes'][att], '%d-%m-%Y %H:%M:%S')
		aa = MarkAttribute.objects.create(mark=mark, field=afield, time_value=d)
  
    success = {}
    
    #change this redirect address if the URL for the application changes
    success['redirect'] = "/portal/animal/view/"+str(mark.animal.id)+"/?action=added_new_mark"
    
    return HttpResponse(json.dumps(success), content_type="application/json")

  #TODO error not ajax or post 
  errors['errors'].append("Bad request, not POST or AJAX")
  return HttpResponse(json.dumps(errors), content_type="application/json")

"""
json_editMark() edits an existing Mark
"""
@login_required
@csrf_exempt
def json_editMark(request):

  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
  'pagemsg': 'Add New Animal',
  }
  dataset_list = AnimalDataset.objects.order_by('name')

  # get user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  #Set time for creation/modification of record
  now = datetime.now()


  m = {}
  errors = {}
  if request.is_ajax() and request.method == 'POST':
    data = json.loads(request.body)
    m = data['mark']
    errors['errors'] = []
    
    
    #validation
    
    if m['mark_type'] =="" or m['mark_type'] =='none':
      errors['errors'].append("Mark Type: You must select a mark type")
    
    if m['mark_number'] == "":
      errors['errors'].append("Mark Number: A Mark ID must be spesified")
    
    #date
    try:
      datetime.strptime(m['mark_date_attached'],'%Y-%m-%d %H:%M:%S')
    except ValueError:
      errors['errors'].append("Date Attached: invalid format. Must be YYYY-MM-DD HH-MM-SS")
    
    date = datetime.strptime(m['mark_date_attached'],'%Y-%m-%d %H:%M:%S')

    if date > now:
      errors['errors'].append("Date Attached: Date Can not be in the future")
      
    if m['mark_is_present'] =="0" or m['mark_is_present'] =="1":

      if (m['mark_is_present'] == "0"):
	is_present=False
      else:
	is_present=True

    else:
      errors['errors'].append("Is Present: Value Invalid")
      
    #TODO validate attributes
    
    #get mark object
    try:
      mark = Mark.objects.get(pk=m['id'])
    except Mark.DoesNotExist:
      errors['errors'].append("Can not find Mark")
      
    if errors['errors']:
      return HttpResponse(json.dumps(errors), content_type="application/json")  
  
    
    #update mark object
    
    if m['mark_type'] != mark.mark_type:
      mark.mark_type = m['mark_type']
    
    if m['mark_number'] != mark.mark_number:
      mark.mark_number = m['mark_number']
      
    if m['mark_date_attached'] !=mark.date_attached:
      mark.date_attached = m['mark_date_attached']
    
    if mark.is_present== True and is_present == False or mark.is_present == False and is_present == True:
      mark.is_present = is_present
	  
    if mark.comments != m['comments']:  
      mark.comments = m['comments']
    
    mark.modified_by = uo
    mark.date_modified = now
    mark.save()
    
    for att in m['attributes']:

	  # grab the attribute
	  af = AttributeField.objects.filter(is_global=True, table='M', name=att)
	  if len(af) == 1:
	      afield = af[0]
	  elif len(af) == 0:
	      af = AttributeField.objects.filter(is_global=False, dataset=cur_ds, table='M', name=att)
	      if len(af) != 1:

		  raise Exception("WTF dataset mark attribute field not found")
	      else:
		  afield = af[0]
	  else:
	      raise Exception("WTF 2 global mark attribute fields with the same name")
	  
	  relation = MarkAttribute.objects.filter(mark=mark, field=afield)
	  if len(relation) == 0:
	      r1 = MarkAttribute(mark=mark, field=afield)
	  else:
	      r1 = relation[0]

	  # if lookup, grab the value
	  if afield.type == 'L':
	      afv = AttributeFieldValue.objects.get(shortval=m['attributes'][att], field=afield)

	      # returning any more than 1 value will raise an exception which is good
	      field=afield 
	      field_value=afv

	      r1.field_value = afv
	  # not a lookup, just store the value in the right field
	  elif afield.type == 'F':
	      field=afield
	      float_value=m['attributes'][att]
	      r1.float_value = float_value
	  elif afield.type == 'T':
	      field=afield 
	      text_value=m['attributes'][att]
	      r1.text_value = text_value
	  elif afield.type == 'I':
	      field=afield
	      int_value=m['attributes'][att]
	      r1.int_value = int_value
	  elif afield.type == 'D':
	      d = datetime.strptime(m['attributes'][att], '%d-%m-%Y %H:%M:%S')
	      field=afield
	      time_value=d
	      r1.time_value = time_value
	  r1.save()
  
    success = {}
    
    #change this redirect address if the URL for the application changes
    success['redirect'] = "/portal/animal/view/"+str(mark.animal.id)+"/?action=updated_mark"
    
    return HttpResponse(json.dumps(success), content_type="application/json")

  #error not ajax or post 
  errors['errors'] = []
  errors['errors'].append("Bad request, not POST or AJAX")
  return HttpResponse(json.dumps(errors), content_type="application/json")