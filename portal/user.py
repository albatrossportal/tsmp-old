from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.views import password_reset, password_reset_confirm
from portal.models import UserProfile, AnimalDataset
from django.core.context_processors import csrf
from django.contrib.auth.hashers import make_password
import json


"""
user_login - Authenticates the user, loads profile, redirects to portal
  if successful.
"""
def user_login(request):
    # Like before, obtain the context for the user's request.
    context = RequestContext(request)

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user is not None:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)

                # Get the user's default dataset from the user profile model
                up = UserProfile.objects.get(user=user)
                if up.default_dataset is None:
                  request.session['cur_ds'] = 1
                else:
                  request.session['cur_ds'] = up.default_dataset.id
                  
                # Finally, if they checked the 'Remember me' checkbox, fix the
                # session expiry variable
                if not request.POST.get('remember_me', None):
                  request.session.set_expiry(0)
                
                # User is logged in, redirect to the portal  
                return HttpResponseRedirect('/portal')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your Rango account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            return HttpResponse("Invalid login details supplied.")

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('index.html', {}, context)

# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/')

@login_required
def index(request):
  # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list

  # Determine if user is admin, if not redirect to no_perms page
  if request.user.is_superuser:
    # Display User List
    users_list = User.objects.order_by('username')
    context_dict['users'] = users_list
    return render_to_response('user_list.html', context_dict, context)
  else:
    # No Permissions Page
    return render_to_response('noperms.html', context_dict, context)

# This is the confirm reset password form
def reset_password_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(request, template_name='reset_password_confirm.html',
        uidb64=uidb64, token=token, post_reset_redirect='/portal')

# This is the password reset form
def reset_password(request):
    return password_reset(request, template_name='reset_password.html',
        email_template_name='reset_password_email.html',
        subject_template_name='reset_password_subject.txt',
        post_reset_redirect='/portal')

# This shows the user detail form
@login_required
def ajax_add_form(request):
  context = RequestContext(request)
  context_dict = {}
  context_dict['adduser'] = True

  dataset_list = AnimalDataset.objects.order_by('name')
  context_dict['datasets'] = dataset_list

  ds_combo_list = []
  for d in dataset_list:
    ds = {}
    ds['id'] = d.id
    ds['name'] = d.name
    ds_combo_list.append(ds)

  context_dict['ds_combolist'] = ds_combo_list
  
  if request.user.is_superuser:
    return render_to_response('user_detail.html', context_dict, context)
  else:
    return render_to_response('noperms.html', context_dict, context)

# This shows the user detail form for a specific user
@login_required
def ajax_edit_form(request, username=None):
  context = RequestContext(request)
  context_dict = {}
  context_dict['adduser'] = False
  
  dataset_list = AnimalDataset.objects.order_by('name')
  context_dict['datasets'] = dataset_list 
  
  if request.user.is_superuser:
    if username is None:
      return HttpResponse("Invalid login details supplied.")
    uo = User.objects.get(username=username)
    ug = uo.groups.all()

    # Prepare a list of datasets combined with user permissions
    user_groups = {}
    for g in ug:
      (g_ds, g_perm) = g.name.split('_', 1)
      user_groups[int(g_ds)] = g_perm

    ds_combo_list = []
    for d in dataset_list:
      ds = {}
      ds['id'] = d.id
      ds['name'] = d.name
      if d.id in user_groups:
        ds['perm'] = user_groups[d.id]
      ds_combo_list.append(ds)

    context_dict['ds_combolist'] = ds_combo_list

    up = UserProfile.objects.get(user=uo)
    context_dict['user_detail'] = uo
    context_dict['user_profile'] = up
    context_dict['user_default_ds'] = up.default_dataset
    context_dict['user_groups'] = user_groups
    context_dict['defaultds'] = up.default_dataset
    context_dict['adduser'] = False
    return render_to_response('user_detail.html', context_dict, context)
  else:
    return render_to_response('noperms.html', context_dict, context)

@login_required
def edit_user(request):
  # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle': 'Threatened Species Portal',
        'pagemsg': 'Add New Animal',
        }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  context_dict['defaultds'] = up.default_dataset

  # Determine if user is admin, if not redirect to no_perms page
  if request.user.is_superuser and request.method == 'POST':
   # FROM VALIDATION
   
   id = request.POST['id']
   
   user_username = request.POST['username']
   user_first_name = request.POST['first_name']
   user_last_name = request.POST['last_name']
   user_email = request.POST['email']
   user_organisation = request.POST['organisation']
   user_phone = request.POST['phone']
   user_password = request.POST['password']
   user_confirm_password = request.POST['confirm_password']
   user_perm = json.loads(request.POST['perm-field'])
 
   if request.POST.get('active', 0):
    user_active = True
   else:
    user_active = False   
   if request.POST.get('administrator', 0):
    user_administrator = True
   else:
    user_administrator = False
   user_default = request.POST.get('default', 0)
      
   ## validation
 
   if user_password != user_confirm_password:
    return HttpResponse("Passwords don't match!") 
   # get the object for the default dataset so we can save it to user profile
   if user_default != 0:
    default_ds = AnimalDataset.objects.get(pk=user_default)

   user_password = make_password(user_password)
   ##if statement here
   if request.POST.get("new"):
     if request.POST['password']=="":
      return HttpResponse("Can not create a user without a password")

   ## add user to database
     u = User(username=user_username, first_name=user_first_name, last_name=user_last_name, email=user_email, password=user_password, is_active=user_active, is_superuser=user_administrator, is_staff=user_administrator)
     u.save()
     if request.POST.get('default_dataset', False):
       user_default_dataset = AnimalDataset.objects.get(id=request.POST['default_dataset'])
       up = UserProfile(user=u, organisation=user_organisation, phone=user_phone, default_dataset=user_default_dataset)
     else:
       user_default_dataset = AnimalDataset.objects.get(id=1)
       #TO DO - the edit user will not render if a default dataset is not picked - however we need to decide what it should be set to if they do not pick one
       up = UserProfile(user=u, organisation=user_organisation, phone=user_phone,default_dataset=user_default_dataset)
     up.save()
     user = User.objects.get(username=user_username)
     user.email=""
     

     #Add user to groups in perm-field
     for p in user_perm:
       g = Group.objects.get(name=p)
       user.groups.add(g)
     user.save()
  

     context_dict['info_msg'] = "Successfully created user"
   else: 
  
     uo = User.objects.get(id=id)
     up = UserProfile.objects.get(user=uo)

     uo.username = user_username
     uo.first_name = user_first_name
     uo.last_name =  user_last_name
     uo.email = user_email
     uo.is_active = user_active
     uo.is_superuser = user_administrator
     uo.is_staff = user_administrator
     if request.POST["password"]!="":
      uo.password = user_password

     up.organisation = user_organisation
     up.phone = user_phone
     if request.POST.get('default_dataset'):
       dds = AnimalDataset.objects.get(id=request.POST['default_dataset'])
       up.default_dataset = dds

     up.save()

     #Clear user's perm groups
     old_perm = uo.groups.all()
     for p in old_perm:
       p.user_set.remove(uo)

     #Add user to groups in perm-field
     for p in user_perm:
       g = Group.objects.get(name=p)
       uo.groups.add(g)
     uo.save()

     context_dict['info_msg'] = "Successfully updated user"
#if ends here
   # Display User List
   users_list = User.objects.order_by('username')
   context_dict['users'] = users_list

   return render_to_response("user_list.html", context_dict, context)
  else:
    return HttpResponse("Not superuser or POST method")
    
