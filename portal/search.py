from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from portal.models import *
from datetime import datetime
from django.contrib import messages
from django.shortcuts import redirect 

@login_required
def index(request):
  # This stuff is on pretty much every page, should really put it in it's own function
  context = RequestContext(request)
  context_dict = { 'pagetitle':	'Threatened Species Portal',
    'pagemsg': 'Search',
    }
  dataset_list = AnimalDataset.objects.order_by('name')

  # shows user details
  uo = request.user
  context_dict['user']= uo
  up = UserProfile.objects.get(user=uo)

  # Grabs current dataset (if not available set it)
  if (request.session.get('cur_ds') is None):
    if (up.default_dataset is None):
      request.session['cur_ds'] = 1
    else:
      request.session['cur_ds'] = up.default_dataset.pk

  # Prepare the context
  cur_ds = AnimalDataset.objects.get(pk=request.session.get('cur_ds'))
  context_dict['cur_ds'] = cur_ds
  context_dict['datasets'] = dataset_list
  mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
  mnums = Mark.get_mark_numbers(request.session.get('cur_ds'))
  if not 'error' in mtypes:
    context_dict['mark_types'] = mtypes['mark_types']
  if not 'error' in mnums:
    context_dict['mark_numbers'] = mnums['mark_numbers']		

  # We've got a form with POST method so update CSRF token
  context_dict.update(csrf(request))
	  
  # Check if the request is someone submitting a form (POST method)
  # If not POST then it's a GET so just display the (blank) form
  if request.method == 'POST':
    band_type = request.POST.get('mark_type', "...")
    markId = request.POST.get('mark_id', "")
    marks = []
    if markId != "" and band_type != "...":
      #search for the animal and add to context dictionary
      band_type = band_type.split()
      try:
        marks = Mark.objects.filter(mark_type=band_type[0], mark_number=markId)
      except Mark.DoesNotExist:
        # Didn't find a mark
        messages.add_message(request, messages.INFO, "Mark Not Found!")
        return render_to_response('search.html', context_dict, context)
      except Mark.MultipleObjectsReturned:
        #Found more than one Mark
        messages.add_message(request, messages.INFO, "ERROR: Found more than one animal with that mark! Select the animal you want from the table below")
        return render_to_response('search.html', context_dict, context)
      
      if len(marks) > 1:
        animals = []
        data = ['x','x']
        for m in marks:
          data[0] = m.animal
          #TO DO - check for dataset constraints
          sa = AttributeField.objects.get(name="Species Common Name")
          sd = AnimalAttribute.objects.get(animal=m.animal, field=sa)
          data[1] = sd.field_value.shortval
          animals.append(data)
          data = ['x','x']

        context_dict['animals'] = animals
        messages.add_message(request, messages.INFO, "ERROR: Found more than one animal with that mark! Select the animal you want from the table below")
        return render_to_response('search.html', context_dict, context)
      
      if len(marks)==0:
        messages.add_message(request, messages.INFO, "Mark Not Found!")
        return render_to_response('search.html', context_dict, context)
      
      else:
	
        animal = marks.first().animal
        
        return redirect('view', aid=animal.id)  

    else:
      if band_type== "...":
        messages.add_message(request, messages.INFO, 'No mark type selected')
      elif markId == "":
        messages.add_message(request, messages.INFO, 'No mark ID detected')
      else:
        messages.add_message(request, messages.INFO, 'Error - please search again')
    return render_to_response('search.html', context_dict, context)
		
	#Is a GET - display blank form
  else:
    mtypes = Mark.get_mark_types(request.session.get('cur_ds'))
    mnums = Mark.get_mark_numbers(request.session.get('cur_ds'))
    if not 'error' in mtypes:
      context_dict['mark_types'] = mtypes['mark_types']
    if not 'error' in mnums:
      context_dict['mark_numbers'] = mnums['mark_numbers']		
    return render_to_response('search.html', context_dict, context)
	
	  
	
