from django.conf import settings
from portal.models import *
from django.core import serializers
from django.contrib.auth.models import User, Group
from zipfile import ZipFile
import zipfile
from datetime import datetime
from os.path import join
import albatross.settings as settings

def backup_all():
    """
    backup_all() - Backs up entire database into JSON objects, saved to disk in a zip file
    """
    fpath = join(settings.MEDIA_ROOT, 'backups', 'fullbackup-' + datetime.now().strftime('%Y-%m-%d-%H%M') + '.zip')
    with ZipFile(fpath, "w", zipfile.ZIP_DEFLATED) as zip:
        zip.writestr('user.json', serializers.serialize("json", User.objects.all()))
        zip.writestr('group.json', serializers.serialize("json", Group.objects.all()))
        zip.writestr('animaldataset.json', serializers.serialize("json", AnimalDataset.objects.all()))
        zip.writestr('userprofile.json', serializers.serialize("json", UserProfile.objects.all()))
        zip.writestr('animal.json', serializers.serialize("json", Animal.objects.all()))
        zip.writestr('mark.json', serializers.serialize("json", Mark.objects.all()))
        zip.writestr('log.json', serializers.serialize("json", Log.objects.all()))
        zip.writestr('breeding.json', serializers.serialize("json", Breeding.objects.all()))
        zip.writestr('file.json', serializers.serialize("json", File.objects.all()))
        zip.writestr('attributefield.json', serializers.serialize("json", AttributeField.objects.all()))
        zip.writestr('attributefieldvalue.json', serializers.serialize("json", AttributeFieldValue.objects.all()))
        zip.writestr('animalattribute.json', serializers.serialize("json", AnimalAttribute.objects.all()))
        zip.writestr('markattribute.json', serializers.serialize("json", MarkAttribute.objects.all()))
        zip.writestr('logattribute.json', serializers.serialize("json", LogAttribute.objects.all()))
        zip.writestr('breedingattribute.json', serializers.serialize("json", BreedingAttribute.objects.all()))
        zip.writestr('queries.json', serializers.serialize("json", Queries.objects.all()))
    b = Backup()
    b.created_time = datetime.now()
    b.backupfile.name = fpath
    b.save()

